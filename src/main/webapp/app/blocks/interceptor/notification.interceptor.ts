import { JhiAlertService } from 'ng-jhipster';
import { HttpInterceptor, HttpRequest, HttpResponse, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import 'rxjs/add/operator/do';

export class NotificationInterceptor implements HttpInterceptor {

    private alertService: JhiAlertService;
    private spinnerService: Ng4LoadingSpinnerService;

    // tslint:disable-next-line: no-unused-variable
    constructor(private injector: Injector) {
        setTimeout(() => {
            this.alertService = injector.get(JhiAlertService);
            this.spinnerService = injector.get(Ng4LoadingSpinnerService);
        });
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		if(this.spinnerService && !this.spinnerService.spinnerSubject.value)
            this.spinnerService.show();

        return next.handle(request).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
				if(this.spinnerService)
					this.spinnerService.hide();
                const arr = event.headers.keys();
                let alert = null;
                let alertParams = null;
                arr.forEach((entry) => {
                    if (entry.endsWith('app-alert')) {
                        alert = event.headers.get(entry);
                    } else if (entry.endsWith('app-params')) {
                        alertParams = event.headers.get(entry);
                    }
                });
                if (alert) {
                    if (typeof alert === 'string') {
                        if (this.alertService) {
                            const alertParam = alertParams;
                            this.alertService.success(alert, { param : alertParam }, null);
                        }
                    }
                }
            }
        }, (err: any) => {
			if(this.spinnerService) this.spinnerService.hide();
        });
    }
}
