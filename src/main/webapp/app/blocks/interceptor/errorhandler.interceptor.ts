import { JhiEventManager } from 'ng-jhipster';
import { HttpInterceptor, HttpRequest, HttpErrorResponse, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injector } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import 'rxjs/add/operator/do';

export class ErrorHandlerInterceptor implements HttpInterceptor {

	spinnerService : Ng4LoadingSpinnerService;

    constructor(private injector: Injector, private eventManager: JhiEventManager) {
        setTimeout(() => {
            this.spinnerService = injector.get(Ng4LoadingSpinnerService);
        });
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).do((event: HttpEvent<any>) => {}, (err: any) => {
            if (err instanceof HttpErrorResponse) {
				
				if(this.spinnerService)
            	            this.spinnerService.hide();
				
                if (!(err.status === 401 && (err.message === '' || (err.url && err.url.indexOf('/api/account') === 0)))) {
                    if (this.eventManager !== undefined) {
                        this.eventManager.broadcast({name: 'variantsApp.httpError', content: err});
                    }
                }
            }
        });
    }
}
