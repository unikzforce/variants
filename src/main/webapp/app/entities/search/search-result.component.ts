import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { SearchResultDetailsComponent } from './search-result-details.component';
import { ITEMS_PER_PAGE } from '../../shared';
import {NgbTooltipConfig} from '@ng-bootstrap/ng-bootstrap';

import * as $ from 'jquery';
import { HttpResponse } from '@angular/common/http';
import { VariantSearchResult, VariantService } from '../variant';

require('tablesorter');

@Component({
    selector: 'jhi-search-result',
    templateUrl: './search-result.component.html',
    styleUrls: ['./search.scss'],
    providers: [NgbTooltipConfig],
})
export class SearchResultComponent implements OnInit {
    
    private subscription: Subscription;
    public criteria: string;
    public currentPage: number = 1;
    public searchPhrase;
    public bundle;
    public filters;
    public totalItems;
    itemsPerPage: any;
    page: any;

    public variantSearchResult: VariantSearchResult[];

    constructor(
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private variantService: VariantService,
        private config: NgbTooltipConfig
    ){
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        config.triggers = 'click';
    }
    
    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.criteria = params['criteria'];
        });
        $("#myTable").tablesorter();
    }

    onVisit() {
        this.searchPhrase = this.variantService.criteria;
        this.criteria = this.variantService.criteria;
        this.bundle = this.variantService.bundle;
        this.filters = this.variantService.filters;
        let req = Object.assign({}, this.variantService.filters);
        let page = this.page == 0 ? 0 : this.page - 1;
        req['variant-search-phrase'] = this.searchPhrase;
        req['page'] = page,
        req['size'] = this.itemsPerPage;
        
        this.variantService.query(req).subscribe((res: HttpResponse<VariantSearchResult[]>) => {
            this.variantSearchResult = res.body;
            this.totalItems = res.headers.get('X-Total-Count');
            // console.log(res.headers.get('X-Total-Count'));
            $(function(){
                $("#myTable").trigger("update"); 
            });
        });
    }

    loadPage(p) {
        this.onVisit();
    }

    open(id: string) {
        let options: NgbModalOptions = {
            size: 'lg'
        }
        const modalRef = this.modalService.open(SearchResultDetailsComponent, options);
        modalRef.componentInstance.id = id;
        // modalRef.componentInstance.name = 'World';
    }

    resetAll() {
        this.page = 0;
        this.variantSearchResult = [];
    }

    splitByComma(str) {
        if(!str) return null;
        let tmp = str.split(',');
        let joined = tmp.join('\n');
        return joined;
    }
}