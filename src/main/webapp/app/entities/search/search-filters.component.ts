import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';
import { Race, RaceService } from '../race';
import { Disease, DiseaseService } from '../disease';
import { ProvinceService } from '../../shared/enum-services/province.service';
import { AuthServerProvider } from '../../shared';
import { Gender } from '../sample/sample.model';
import { HttpResponse } from '@angular/common/http';
import { VariantService } from '../variant';

@Component({
    selector: 'jhi-search-filters',
    templateUrl: './search-filters.component.html',
    styleUrls: ['./search.scss'],
})
export class SearchFiltersComponent implements OnInit {
    
    private subscription: Subscription;
    public criteria: string;
    public ageRange: number[] = [0, 100];

    genderList = [];
    racesList = [];
    diseasesList = [];
    provincesList = [];
    
    selectedGenders = [];
    selectedRaces = [];
    selectedDiseases = [];
    selectedProvinces = [];
    dropdownSettings = {};

    constructor(
        private route: ActivatedRoute,
        private raceService: RaceService,
        private diseaseService: DiseaseService,
        private provinceService: ProvinceService,
        private variantService: VariantService
    ){}

    convertRaceListToMultiList(list: any[]) {
        let resList = [];
        for(let i =0 ; i < list.length ; i++) {
            resList.push({
                id: list[i].id,
                itemName: list[i].name
            });
        }
        return resList;
    }

    getItemIDs(itemList: any[]): number[]{
        let IDs: number[] = [];
        itemList.forEach(item => {
            IDs.push(item.id);
        });
        return IDs;
    }

    getItemNames(itemList: any[]): string[]{
        let names: string[] = [];
        itemList.forEach(item => {
            names.push(item.itemName);
        });
        return names;
    }
    
    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.criteria = params['criteria'];
        });

        this.raceService.query()
        .subscribe((res: HttpResponse<Race[]>) => { this.racesList = this.convertRaceListToMultiList(res.body); });
        
        this.diseaseService.query()
            .subscribe((res: HttpResponse<Disease[]>) => { this.diseasesList = this.convertRaceListToMultiList(res.body); });
        
        this.provincesList = this.provinceService.getProvinceItemList();


        this.fetchGenders();
        
        this.dropdownSettings = { 
            singleSelection: false, 
            text:"Choose..",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };
    }

    public fetchGenders() {
        this.genderList = [];
        for(let e in Gender){
            if(isNaN(Number(e))){
                this.genderList.push({
                    id: Gender[e],
                    itemName: e
                });
            }
        }
    }

    beforeSearch() {
        this.variantService.filters = {
            "sample-age-start": this.ageRange[0],
            "sample-age-end": this.ageRange[1],
            'sample-genders[]': this.getItemIDs(this.selectedGenders),
            'sample-birth-provinces[]': this.getItemIDs(this.selectedProvinces),
            'sample-disease-ids[]': this.getItemIDs(this.selectedDiseases),
            'sample-race-ids[]': this.getItemIDs(this.selectedRaces),
        };

        this.variantService.bundle = {
            genders: this.getItemNames(this.selectedGenders),
            provinces: this.getItemNames(this.selectedProvinces),
            diseases: this.getItemNames(this.selectedDiseases),
            races: this.getItemNames(this.selectedRaces),
        };
    }

    resetAll() {
        this.selectedGenders = [];
        this.selectedRaces = [];
        this.selectedDiseases = [];
        this.selectedProvinces = [];
        this.ageRange = [0, 100];
        this.fetchGenders();
    }
}