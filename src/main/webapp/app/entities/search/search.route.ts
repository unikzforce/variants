import { SearchComponent } from './search.component';
import { UserRouteAccessService } from "../../shared";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { SearchFiltersComponent } from './search-filters.component';
import { SearchResultComponent } from './search-result.component';
import { SearchContainerComponent } from './search-container.component';

export const searchRoute: Routes = [
    {
        path: 'search',
        component: SearchContainerComponent,
        resolve: {
            // 'pagingParams': SampleResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'variantsApp.sample.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'search/:criteria',
        component: SearchFiltersComponent,
        resolve: {
            // 'pagingParams': SampleResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'variantsApp.sample.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'search/:criteria/result',
        component: SearchResultComponent,
        resolve: {
            // 'pagingParams': SampleResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'variantsApp.sample.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
