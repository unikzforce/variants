import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { VariantsSharedModule, ShrinkColumnComponent } from '../../shared';
import {
    SearchComponent,
    searchRoute,
    SearchFiltersComponent,
    SearchResultComponent,
    SearchContainerComponent,
    SearchResultDetailsComponent,
    CommaTruncatePipe,
    ShrinkFractionPipe
} from './';

import { ArchwizardModule } from 'ng2-archwizard';
import { NouisliderModule } from 'ng2-nouislider';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgbPaginationModule, NgbModalModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
// import { TruncateModule } from 'ng2-truncate';


const ENTITY_STATES = [
    ...searchRoute,
    // ...samplePopupRoute,
];

@NgModule({
    imports: [
        VariantsSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        ArchwizardModule,
        NouisliderModule,
        AngularMultiSelectModule,
        NgbPaginationModule.forRoot(),
        NgbModalModule.forRoot(),
        NgbTabsetModule.forRoot(),
        ChartsModule,
        // TruncateModule
    ],
    declarations: [
        SearchComponent,
        SearchFiltersComponent,
        SearchResultComponent,
        SearchContainerComponent,
        SearchResultDetailsComponent,
        CommaTruncatePipe,
        ShrinkFractionPipe,
    ],
    entryComponents: [
        SearchResultDetailsComponent
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SearchModule {}
