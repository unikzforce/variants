import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Principal } from '../../shared';
import { VariantService } from '../variant';

@Component({
    selector: 'jhi-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.scss'],
})
export class SearchComponent implements OnInit {
    
    @Input() wizard;
    currentAccount: any;
    criteria: string;

    constructor(
        private principal: Principal,
        private router: Router,
        private variantService: VariantService
    ){

    }
    
    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    go() {
        this.variantService.criteria = this.criteria;
        this.wizard.navigation.goToStep(1);
    }
}