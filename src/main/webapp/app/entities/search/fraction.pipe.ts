import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'shrinkfraction'
})

export class ShrinkFractionPipe implements PipeTransform {
    
    truncate (num, places) {
        return Math.trunc(num * Math.pow(10, places)) / Math.pow(10, places);
    }

    transform(value: number, limit: number): any {
        if(!limit) return value;
        let splitted = (value + '').split(".");

        if(splitted.length<2){
            return value;
        } else{
            let count = splitted[1].length;
            if(count <= limit)    return value;
            else {
                console.log(Math.pow(10, -limit));
                if(value < Math.pow(10, -limit)){
                    return "< e^-" + limit;
                } else {
                    return this.truncate(value, limit);
                }
            }
        } 
   }
}