import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Principal } from '../../shared';
import { WizardComponent } from 'ng2-archwizard'
import { SearchFiltersComponent } from './search-filters.component';
import { SearchResultComponent } from './search-result.component';

@Component({
    selector: 'jhi-search-container',
    templateUrl: './search-container.component.html',
    styleUrls: ['./search.scss'],
})
export class SearchContainerComponent implements OnInit {
    
    @ViewChild(SearchFiltersComponent) searchFiltersComponent: SearchFiltersComponent;
    @ViewChild(SearchResultComponent) searchResultComponent: SearchResultComponent;
    
    currentAccount: any;
    criteria: string;

    @ViewChild(WizardComponent)
    public wizard: WizardComponent;

    constructor(
        private principal: Principal,
        private router: Router,
    ){
        
    }
    
    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            // console.log(this.wizard.navigation.goToStep);
            this.setCircleListeners();
        });
    }

    setCircleListeners() {
        let circles = document.querySelectorAll('wizard-navigation-bar li');
        let parent = this;
        for(let i =0; i < circles.length -1 ; i++) {
            circles[i].addEventListener('click', function(e){
                parent.wizard.navigation.goToStep(i);
            });
        }
    }

    // go() {
    //     console.log(this.criteria);
    //     this.router.navigate(['search/'+this.criteria]);
    // }

    enableBlink(ev){
        let el = document.querySelector('wizard-navigation-bar li:last-child');
        el.classList.add('blink_me');

        let parent = this;
        document.querySelector('.blink_me').addEventListener('click', function(e){
            parent.searchFiltersComponent.beforeSearch();
            parent.wizard.navigation.goToStep(2);
            
        });
        
    }

    disableBlink(ev) {
        let el = document.querySelector('wizard-navigation-bar li:last-child');
        el.classList.remove('blink_me');
        let elClone = el.cloneNode(true);
        el.parentNode.replaceChild(elClone, el);
    }

    enableReset(e) {
        let el = document.querySelector('wizard-navigation-bar li:last-child');            
        el.classList.add('reset_me');
        this.searchResultComponent.onVisit();
        let parent = this;
        document.querySelector('.reset_me').addEventListener('click', function(e){
            parent.criteria = null;
            parent.searchFiltersComponent.resetAll();
            parent.searchResultComponent.resetAll();
            parent.wizard.navigation.goToStep(0);
        });
    }

    disableReset(e) {
        let el = document.querySelector('wizard-navigation-bar li:last-child');
        el.classList.remove('reset_me');
        let elClone = el.cloneNode(true);
        el.parentNode.replaceChild(elClone, el);
        // this.searchFiltersComponent.resetAll();
        this.searchResultComponent.resetAll();
    }
}