import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'commatruncate'
})
export class CommaTruncatePipe implements PipeTransform {
    transform(value: string, args: string[]): string {
        if(!value) return null;
        let splitted = value.split(',');
        const limit = args.length > 0 ? parseInt(args[0], 10) : 20;
        const trail = args.length > 1 ? args[1] : '...';
        return splitted.length > 1 ? splitted[0] + trail : value;
   }
}