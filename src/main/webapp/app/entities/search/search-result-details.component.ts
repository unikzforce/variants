import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import * as $ from 'jquery';
import { Sample, SampleWithVariantZygosity } from '../sample/sample.model';
import { HttpResponse } from '@angular/common/http';
import { SampleService } from '../sample';
import { Variant, VariantSampleDiseasePrevalence, VariantService } from '../variant';
require('tablesorter');

@Component({
    selector: 'jhi-search-result-details',
    templateUrl: './search-result-details.component.html',
    styleUrls: ['./search.scss'],
})
export class SearchResultDetailsComponent implements OnInit {
    
    public id: string;
    public variant: Variant;
    public prevalenceList: VariantSampleDiseasePrevalence;
    public showPlot: boolean = false;
    public samples: SampleWithVariantZygosity[];

    constructor(
        private activeModal: NgbActiveModal,
        private variantService: VariantService,
        private sampleService: SampleService
    ){
    }
    
    ngOnInit() {
        this.variantService.find(this.id).subscribe(
            (res: HttpResponse<Variant>) => { this.variant = res.body;}
        );
    }

    close() {
        this.activeModal.close();
    }

    public barChartOptions:any = {
        scaleShowVerticalLines: false,
        responsive: true,
        scales: {
            xAxes: [{
                maxBarThickness: 50,
                // display: false,
                // stacked: true,
            }],
            yAxes: [{ 
                scaleLabel: {
                    display: true,
                    labelString: "Sample Count"
                },
                ticks: {
                    suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                }
            }]
        }
    };
    public barChartLabels:string[] = [];
    public barChartType:string = 'bar';
    public barChartLegend:boolean = true;
    
    public barChartData:any[] = [
        {data: [], label: 'Frequency'}
    ];

    convertPrevalenceToArray(prevalenceList: VariantSampleDiseasePrevalence) {
        if(!prevalenceList.sampleCount) prevalenceList.sampleCount=1;
        prevalenceList.prevalences.forEach(prevalence => {
            this.barChartData[0].data.push(prevalence.prevalence);///prevalenceList.sampleCount);
            this.barChartLabels.push(prevalence.name);
        });
        this.showPlot = true;
    }

    public chartClicked(e:any):void {
        let idx = e.active[0]._index;
        this.sampleService
            .findAllByVariantIdAndDiseaseId(this.variant.id, this.prevalenceList.prevalences[idx].id)
            .subscribe((res: HttpResponse<SampleWithVariantZygosity[]>) => {
                this.samples = res.body;
                
                $(function(){
                    $("table").tablesorter();
                    $("table").trigger("update"); 
                });
            });
    }
    
    public chartHovered(e:any):void {
    }
    
    onTabChange(e) {
        if(e.nextId == 'prevalence' && !this.prevalenceList) {
            this.variantService.getPrevelance(this.id).subscribe((res: HttpResponse<VariantSampleDiseasePrevalence>) => {
                this.prevalenceList = res.body;
                this.convertPrevalenceToArray(this.prevalenceList);
                this.barChartOptions.scales.yAxes[0].scaleLabel.labelString = "Sample Count (Total = " + this.prevalenceList.prevalences.length + " )";
            });
        }
    }
}