import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SearchModule } from './search/search.module';
import { VariantsRaceModule } from './race/race.module';
import { VariantsDiseaseModule } from './disease/disease.module';
import { VariantsSampleModule } from './sample/sample.module';
import { VariantsVariantModule } from './variant/variant.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        SearchModule,
        VariantsRaceModule,
        VariantsDiseaseModule,
        VariantsSampleModule,
        VariantsVariantModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VariantsEntityModule {}
