import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Race } from './race.model';
import { RacePopupService } from './race-popup.service';
import { RaceService } from './race.service';

@Component({
    selector: 'jhi-race-dialog',
    templateUrl: './race-dialog.component.html'
})
export class RaceDialogComponent implements OnInit {

    race: Race;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private raceService: RaceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.race.id !== undefined) {
            this.subscribeToSaveResponse(
                this.raceService.update(this.race));
        } else {
            this.subscribeToSaveResponse(
                this.raceService.create(this.race));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Race>>) {
        result.subscribe((res: HttpResponse<Race>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Race) {
        this.eventManager.broadcast({ name: 'raceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-race-popup',
    template: ''
})
export class RacePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private racePopupService: RacePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.racePopupService
                    .open(RaceDialogComponent as Component, params['id']);
            } else {
                this.racePopupService
                    .open(RaceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
