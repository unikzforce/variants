import { BaseEntity } from './../../shared';

export class Race implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
