import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Race } from './race.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Race>;

@Injectable()
export class RaceService {

    private resourceUrl =  SERVER_API_URL + 'api/races';

    constructor(private http: HttpClient) { }

    create(race: Race): Observable<EntityResponseType> {
        const copy = this.convert(race);
        return this.http.post<Race>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(race: Race): Observable<EntityResponseType> {
        const copy = this.convert(race);
        return this.http.put<Race>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Race>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Race[]>> {
        const options = createRequestOption(req);
        return this.http.get<Race[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Race[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Race = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Race[]>): HttpResponse<Race[]> {
        const jsonResponse: Race[] = res.body;
        const body: Race[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Race.
     */
    private convertItemFromServer(race: Race): Race {
        const copy: Race = Object.assign({}, race);
        return copy;
    }

    /**
     * Convert a Race to a JSON which can be sent to the server.
     */
    private convert(race: Race): Race {
        const copy: Race = Object.assign({}, race);
        return copy;
    }
}
