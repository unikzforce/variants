import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FileUploadModule } from 'ng2-file-upload';

import { VariantsSharedModule } from '../../shared';
import {
    DiseaseService,
    DiseasePopupService,
    DiseaseComponent,
    DiseaseDetailComponent,
    DiseaseDialogComponent,
    DiseasePopupComponent,
    DiseaseDeletePopupComponent,
    DiseaseDeleteDialogComponent,
    diseaseRoute,
    diseasePopupRoute,
    DiseaseResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...diseaseRoute,
    ...diseasePopupRoute,
];

@NgModule({
    imports: [
        VariantsSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        FileUploadModule,
    ],
    declarations: [
        DiseaseComponent,
        DiseaseDetailComponent,
        DiseaseDialogComponent,
        DiseaseDeleteDialogComponent,
        DiseasePopupComponent,
        DiseaseDeletePopupComponent,
    ],
    entryComponents: [
        DiseaseComponent,
        DiseaseDialogComponent,
        DiseasePopupComponent,
        DiseaseDeleteDialogComponent,
        DiseaseDeletePopupComponent,
    ],
    providers: [
        DiseaseService,
        DiseasePopupService,
        DiseaseResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VariantsDiseaseModule {}
