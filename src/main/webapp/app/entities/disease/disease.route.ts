import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { DiseaseComponent } from './disease.component';
import { DiseaseDetailComponent } from './disease-detail.component';
import { DiseasePopupComponent } from './disease-dialog.component';
import { DiseaseDeletePopupComponent } from './disease-delete-dialog.component';

@Injectable()
export class DiseaseResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const diseaseRoute: Routes = [
    {
        path: 'disagnosis',
        component: DiseaseComponent,
        resolve: {
            'pagingParams': DiseaseResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'variantsApp.disease.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'disagnosis/:id',
        component: DiseaseDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'variantsApp.disease.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const diseasePopupRoute: Routes = [
    {
        path: 'disagnosis-new',
        component: DiseasePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'variantsApp.disease.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'disagnosis/:id/edit',
        component: DiseasePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'variantsApp.disease.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'disagnosis/:id/delete',
        component: DiseaseDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'variantsApp.disease.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
