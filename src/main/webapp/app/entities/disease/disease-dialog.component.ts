import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { AuthServerProvider } from '../../shared';

import { Disease } from './disease.model';
import { DiseasePopupService } from './disease-popup.service';
import { DiseaseService } from './disease.service';
import { FileUploader } from 'ng2-file-upload';

import * as $ from 'jquery';

@Component({
    selector: 'jhi-disease-dialog',
    templateUrl: './disease-dialog.component.html'
})
export class DiseaseDialogComponent implements OnInit {

    disease: Disease;
    isSaving: boolean;
    URL: string = "/api/diseases/upload";
    public uploader: FileUploader = new FileUploader({url: this.URL, parametersBeforeFiles: true});

    constructor(
        public activeModal: NgbActiveModal,
        private diseaseService: DiseaseService,
        private eventManager: JhiEventManager,
        private authServiceProvider: AuthServerProvider,        
    ) {
    }

    ngOnInit() {
        this.isSaving = false;

        this.uploader.onBeforeUploadItem  = (fileItem) => {
            fileItem.headers.push({name: 'Authorization', value: "Bearer " + this.authServiceProvider.getToken()});
            return fileItem;
        };

        this.uploader.onCompleteItem = (item, response, status, header) => {
            if (status >= 200 && status <300) {
                this.onSaveSuccess(null);
            }else{
                console.log(item, response, status, header);
            }  
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.disease.id !== undefined) {
            this.subscribeToSaveResponse(
                this.diseaseService.update(this.disease));
        } else {
            if(this.uploader.queue.length > 0)
                this.uploader.uploadAll();
            else this.subscribeToSaveResponse(
                this.diseaseService.create(this.disease));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Disease>>) {
        result.subscribe((res: HttpResponse<Disease>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Disease) {
        this.eventManager.broadcast({ name: 'diseaseListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    updateFileInputText(el){
        el = el.target;
        $(el).next().after().text($(el).val().split('\\').slice(-1)[0]);
    }
}

@Component({
    selector: 'jhi-disease-popup',
    template: ''
})
export class DiseasePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private diseasePopupService: DiseasePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.diseasePopupService
                    .open(DiseaseDialogComponent as Component, params['id']);
            } else {
                this.diseasePopupService
                    .open(DiseaseDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
