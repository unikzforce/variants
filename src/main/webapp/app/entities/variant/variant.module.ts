import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { VariantsSharedModule } from '../../shared';

import {
    VariantService
} from './variant.service';

@NgModule({
    imports: [
        VariantsSharedModule,
    ],
    declarations: [
    ],
    entryComponents: [
    ],
    providers: [
        VariantService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VariantsVariantModule {}