import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { createRequestOption } from '../../shared';
import { Variant, VariantSampleDiseasePrevalence, VariantSearchResult } from './variant.model';
import { HttpResponse, HttpClient, HttpParams } from "@angular/common/http";
import { EntityResponseType } from "../disease";

@Injectable()
export class VariantService {

    criteria;
    filters;
    bundle;

    private resourceUrl = SERVER_API_URL + 'api/variants';

    constructor(private http: HttpClient) { }

    query(req?: any): Observable<HttpResponse<VariantSearchResult[]>> {
        const options = createRequestOption(req);
        return this.http.get<VariantSearchResult[]>(`${this.resourceUrl}/search`, { params: options, observe: 'response' })
            .map((res: HttpResponse<VariantSearchResult[]>) => this.convertArrayResponse(res));
    }

    private convertArrayResponse(res: HttpResponse<VariantSearchResult[]>): HttpResponse<VariantSearchResult[]> {
        const jsonResponse: VariantSearchResult[] = res.body;
        const body: VariantSearchResult[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertVariantLightItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

        /**
     * Convert a returned JSON object to VariantLight.
     */
    private convertVariantLightItemFromServer(variantLight: VariantSearchResult): VariantSearchResult {
        const copy: VariantSearchResult = Object.assign({}, variantLight);
        return copy;
    }

    // ---------------------------------

    find(id: string): Observable<HttpResponse<Variant>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: HttpResponse<Variant>) => this.convertVariantResponse(res));
    }

    private convertVariantResponse(res: HttpResponse<Variant>): HttpResponse<Variant> {
        const body: Variant = this.convertVariantItemFromServer(res.body);
        return res.clone({body});
    }

   private convertVariantItemFromServer(variant: Variant): Variant {
        const copy: Variant = Object.assign({}, variant);
        return copy;
    }
 
    // ---------------------------------

    getPrevelance(id: string): Observable<HttpResponse<VariantSampleDiseasePrevalence>> {
        return this.http.get(`${this.resourceUrl}/${id}/extra`, { observe: 'response'})
        .map((res: HttpResponse<VariantSampleDiseasePrevalence>) =>  this.convertPrevalenceResponse(res));
    }

    private convertPrevalenceResponse(res: HttpResponse<VariantSampleDiseasePrevalence>): HttpResponse<VariantSampleDiseasePrevalence> {
        const body: VariantSampleDiseasePrevalence = this.convertPrevalenceListItemFromServer(res.body);
        return res.clone({body});
    }

    private convertPrevalenceListItemFromServer(prevalenceList: VariantSampleDiseasePrevalence): VariantSampleDiseasePrevalence {
        const copy: VariantSampleDiseasePrevalence = Object.assign({}, prevalenceList);
        return copy;
    }


}