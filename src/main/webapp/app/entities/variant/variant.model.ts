import { BaseEntity } from './../../shared';

export class VariantSearchResult implements BaseEntity {
    constructor(
        public id?: string,
        public chr?: string,
        public position?: number,
        public ref?: string,
        public alt?: string,
        public refGeneFunc?: string,
        public refGene?: string,
        public refGeneExonicFunc?: string,
        public alleleFrequency?: number,
    ) {
    }
}

export class Variant implements BaseEntity {
    constructor(
        public id?: string,
        public chr?: string,
        public position?: number,
        public ref?: string,
        public alt?: string,
        public refGeneFunc?: string,
        public refGene?: string,
        public refGeneExonicFunc?: string,
        public alleleFrequency?: number,
        public refGeneAAChange?: string,
        public esp6500SIV2All?: string,
        public the1000g2015AugAll?: string,
        public exacAll?: string,
        public cg69?: string,
        public snp142?: string,
        public clinsig?: string,
        public clndbn?: string,
        public clnacc?: string,
        public clndsdbid?: string,
        public siftScore?: string,
        public polyphen2HDIVScore?: string,
        public caddRaw?: string,
        public caddPhred?: string,
    ) {
    }
}

export class DiseasePrevalence implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public prevalence?: number,
    ) {
    }
}

export class VariantSampleDiseasePrevalence implements BaseEntity {
    constructor(
        public id?: number,
        public sampleCount?: number,
        public prevalences?: DiseasePrevalence[],
    ){

    }
}
