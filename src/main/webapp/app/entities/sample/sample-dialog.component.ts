import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDateUtils } from 'ng-jhipster';
import { Sample } from './sample.model';
import { SamplePopupService } from './sample-popup.service';
import { SampleService } from './sample.service';
import { Race, RaceService } from '../race';
import { Disease, DiseaseService } from '../disease';

import { AuthServerProvider } from '../../shared';
import { FileUploader } from 'ng2-file-upload';
import { ProvinceService } from '../../shared/enum-services/province.service';
import { Gender } from './sample.model';

import * as $ from 'jquery';
import { Province } from '../../shared/enums/province.enum';

@Component({
    selector: 'jhi-sample-dialog',
    templateUrl: './sample-dialog.component.html',
    styleUrls: ['./sample.scss']
})
export class SampleDialogComponent implements OnInit {

    sample: Sample;
    isSaving: boolean;

    races: Race[];

    diseases: Disease[];
    birthDateDp: any;
    deathDateDp: any;

    genderList = [];
    racesList = [];
    diseasesList = [];
    provincesList = [];
    
    selectedGender;
    selectedRaces = [];
    selectedDiseases = [];
    selectedBirthProvince;
    selectedLivingProvince;
    dropdownSettingsMulti = {};
    dropdownSettingsSingle = {};
    URL: string = "/api/samples/upload-sample";

    public uploader: FileUploader = new FileUploader({url: this.URL, parametersBeforeFiles: true});

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private sampleService: SampleService,
        private raceService: RaceService,
        private diseaseService: DiseaseService,
        private eventManager: JhiEventManager,
        private authServiceProvider: AuthServerProvider,
        private provinceService: ProvinceService,
        private dateUtils: JhiDateUtils,
    ) {
    }

    convertRaceListToMultiList(list: any[]) {
        let resList = [];
        for(let i =0 ; i < list.length ; i++) {
            resList.push({
                id: list[i].id,
                itemName: list[i].name
            });
        }
        return resList;
    }

    getItemIDs(itemList: any[]): number[]{
        let IDs: number[] = [];
        itemList.forEach(item => {
            IDs.push(item.id);
        });
        return IDs;
    }

    ngOnInit() {
        this.isSaving = false;
        this.raceService.query()
            .subscribe((res: HttpResponse<Race[]>) => { this.races = res.body; this.racesList = this.convertRaceListToMultiList(this.races); }, (res: HttpErrorResponse) => this.onError(res.message));
        this.diseaseService.query()
            .subscribe((res: HttpResponse<Disease[]>) => { this.diseases = res.body; this.diseasesList = this.convertRaceListToMultiList(this.diseases); }, (res: HttpErrorResponse) => this.onError(res.message));
        
        this.provincesList = this.provinceService.getProvinceItemList();

        this.uploader.onBeforeUploadItem  = (fileItem) => {
            fileItem.headers.push({name: 'Authorization', value: "Bearer " + this.authServiceProvider.getToken()});
            return fileItem;
        };

        this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
            form.append('firstName', this.sample.firstName);
            form.append('lastName', this.sample.lastName);
            form.append('age', this.sample.age);
            form.append('nationalCode', this.sample.nationalCode);
            // form.append('birthDate', this.dateUtils.convertLocalDateToServer(this.sample.birthDate));
            // form.append('deathDate', this.dateUtils.convertLocalDateToServer(this.sample.deathDate));
            // form.append('gender', this.selectedGender[0].id);
            form.append('gender', Gender[this.selectedGender[0].id]);
            form.append('birthProvince', Province[this.selectedBirthProvince[0].id]);
            form.append('livingProvince', Province[this.selectedLivingProvince[0].id]);
            form.append('races', this.getItemIDs(this.selectedRaces));
            form.append('diseases', this.getItemIDs(this.selectedDiseases));
        }

        this.uploader.onCompleteItem = (item, response, status, header) => {
            if (status >= 200 && status <300) {
              //Your code goes here
              this.eventManager.broadcast({ name: 'sampleListModification', content: 'OK'});
              this.activeModal.dismiss();
            }else{
                console.log(item, response, status, header);
            }  
        }

        this.fetchGenders();
        // this.fetchRaces();
        // this.fetchDiseases();
        // this.fetchProvinces();
        
        this.dropdownSettingsMulti = { 
            singleSelection: false, 
            text:"Choose..",
            selectAllText:'Select All',
            unSelectAllText:'Unselect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        this.dropdownSettingsSingle = JSON.parse(JSON.stringify(this.dropdownSettingsMulti));
        this.dropdownSettingsSingle['singleSelection'] = true;

        if(this.sample.id){
            this.preSelect();
        }

        
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.sample.id !== undefined) {
            this.subscribeToSaveResponse(
                this.raceService.update(this.sample));
        } else {
            this.subscribeToSaveResponse(
                this.raceService.create(this.sample));
        }
    }

    uploadFileAndMetadata() {
        this.isSaving = true;
        if (this.sample.id !== undefined) {
            this.sample.gender = this.selectedGender[0].itemName;
            this.sample.birthProvince = this.selectedBirthProvince[0].itemName;
            this.sample.livingProvince = this.selectedLivingProvince[0].itemName;
            this.sample.races = this.selectedRaces;
            this.sample.diseases = this.selectedDiseases;
            this.subscribeToSaveResponse(this.sampleService.update(this.sample));
        } else {
            console.log(this.uploader);
            this.uploader.uploadAll();
            
            // this.subscribeToSaveResponse(this.sampleService.create(this.sample));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Sample>>) {
        result.subscribe((res: HttpResponse<Sample>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Sample) {
        this.eventManager.broadcast({ name: 'sampleListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRaceById(index: number, item: Race) {
        return item.id;
    }

    trackDiseaseById(index: number, item: Disease) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }

    public fetchGenders() {
        for(let e in Gender){
            if(isNaN(Number(e))){
                this.genderList.push({
                    id: Gender[e],
                    itemName: e
                });
            }
        }
        console.log(this.genderList);
    }

    public fetchRaces() {
        this.racesList = [
            {
                id: 1,
                itemName:"White"
            },{
                id: 2,
                itemName:"Asian"
            },{
                id: 3,
                itemName:"Other"
            },
        ];
        
        // this.selectedRaces = this.racesList.map(x => Object.assign({}, x));
    }

    public fetchDiseases() {
        this.diseasesList = [
            {
                id: 1,
                itemName:"Adult Cold"
            },{
                id: 2,
                itemName:"Beri Beri"
            },{
                id: 3,
                itemName:"Other"
            },
        ];
        
        // this.selectedDiseases = this.diseasesList.map(x => Object.assign({}, x));
    }

    public fetchProvinces() {
        this.provincesList = [
            {
                id: 1,
                itemName:"Tehran"
            },{
                id: 2,
                itemName:"Tabriz"
            },{
                id: 3,
                itemName:"Other"
            },
        ];
        
        // this.selectedProvinces = this.provincesList.map(x => Object.assign({}, x));
    }

    updateFileInputText(el){
        el = el.target;
        $(el).next().after().text($(el).val().split('\\').slice(-1)[0]);
    }

    preSelect() {
        this.selectedGender = [{
            id: Gender[this.sample.gender],
            itemName: this.sample.gender
        }];

        this.selectedBirthProvince = [{
            id: Province[this.sample.birthProvince],
            itemName: this.sample.birthProvince
        }];

        this.selectedLivingProvince = [{
            id: Province[this.sample.livingProvince],
            itemName: this.sample.livingProvince
        }];

        this.sample.diseases.forEach(disease => {
            this.selectedDiseases.push({
                id: disease.id,
                itemName: disease.name
            });
        });
        
        this.sample.races.forEach(race => {
            this.selectedRaces.push({
                id: race.id,
                itemName: race.name
            });
        });
    }
}

@Component({
    selector: 'jhi-sample-popup',
    template: ''
})
export class SamplePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private samplePopupService: SamplePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.samplePopupService
                    .open(SampleDialogComponent as Component, params['id']);
            } else {
                this.samplePopupService
                    .open(SampleDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
