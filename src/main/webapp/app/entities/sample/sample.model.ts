import { BaseEntity } from './../../shared';
import { Disease } from '../disease/disease.model';
import { Race } from '../race/race.model';

export enum Gender {
    MALE = 1,
    FEMALE = 2,
    OTHER = 3
}

export const enum Province {
    ALBORZ = 1,
    ARDABIL = 2,
    AZERBAIJAN_EAST = 3,
    AZERBAIJAN_WEST = 4,
    BUSHEHR = 5,
    CHAHAR_MAHAL_AND_BAKHTIARI = 6,
    FARS = 7,
    GILAN = 8,
    GOLESTAN = 9,
    HAMADAN = 10,
    HORMOZGAN = 11,
    ILAM = 12,
    ISFAHAN = 13,
    KERMAN = 14,
    KERMANSHAH = 15,
    KHORASAN_NORTH = 16,
    KHORASAN_RAZAVI = 17,
    KHORASAN_SOUTH = 18,
    KHUZESTAN = 19,
    KOHGILUYEH_AND_BOYER_AHMAD = 20, 
    KURDESTAN = 21,
    LORESTAN = 22,
    MARKAZI = 23,
    MAZANDARAN = 24,
    QAZVIN = 25,
    QOM = 26,
    SEMNAN = 27,
    SISTAN_AND_BALUCHESTAN = 28,
    TEHRAN = 29,
    YAZD = 30,
    ZANJAN =31
}

export const enum SampleImportingStatus {
    'NOT_STARTED',
    'NOT_FINISHED',
    'FINISHED',
    'FAILED',
    'FAILURE_CLEAN_UP'
}

export const enum Zygosity {
    'HOMOZYGOUS',
    'HETEROZYGOUS',
    'HEMIZYGOUS',
    'HET_COM',
    'NULLYZIGOUS'
}

export class Sample implements BaseEntity {
    constructor(
        public id?: number,
        public fileOriginalName?: string,
        public fileHashName?: string,
        public firstName?: string,
        public lastName?: string,
        public age?: number,
        public nationalCode?: string,
        public birthDate?: any,
        public deathDate?: any,
        public gender?: Gender,
        public birthProvince?: Province,
        public livingProvince?: Province,
        public importingStatus?: SampleImportingStatus,
        public races?: Race[],
        public diseases?: Disease[],
    ) {
    }
}

export class SampleWithVariantZygosity implements BaseEntity {
    constructor(
        public id?: number,
        public fileOriginalName?: string,
        public fileHashName?: string,
        public firstName?: string,
        public lastName?: string,
        public age?: number,
        public nationalCode?: string,
        public birthDate?: any,
        public deathDate?: any,
        public gender?: Gender,
        public birthProvince?: Province,
        public livingProvince?: Province,
        public importingStatus?: SampleImportingStatus,
        public races?: Race[],
        public diseases?: Disease[],
        public variantZygosity?: Zygosity
    ) {
    }   
}
