import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Sample, SampleWithVariantZygosity } from './sample.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Sample>;

@Injectable()
export class SampleService {

    private resourceUrl =  SERVER_API_URL + 'api/samples';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(sample: Sample): Observable<EntityResponseType> {
        const copy = this.convert(sample);
        return this.http.post<Sample>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(sample: Sample): Observable<EntityResponseType> {
        const copy = this.convert(sample);
        return this.http.put<Sample>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Sample>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Sample[]>> {
        const options = createRequestOption(req);
        return this.http.get<Sample[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Sample[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Sample = this.convertItemFromServer(res.body);
        return res.clone({body});
    }


    findAllByVariantIdAndDiseaseId(variantId:string, diseaseId:number): Observable<HttpResponse<SampleWithVariantZygosity[]>> {
        let options: HttpParams = new HttpParams();
        options = options.set('variantId', variantId);
        options = options.set('diseaseId', diseaseId.toString());
        return this.http.get(`${this.resourceUrl}/variant-disease-info` , { params: options, observe: 'response'})
        .map((res: HttpResponse<SampleWithVariantZygosity[]>) => this.convertArrayResponseWithZygosity(res));
    }

    private convertArrayResponseWithZygosity(res: HttpResponse<SampleWithVariantZygosity[]>): HttpResponse<SampleWithVariantZygosity[]> {
        const jsonResponse: SampleWithVariantZygosity[] = res.body;
        const body: SampleWithVariantZygosity[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServerWithZygosity(jsonResponse[i]));
        }
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Sample[]>): HttpResponse<Sample[]> {
        const jsonResponse: Sample[] = res.body;
        const body: Sample[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Sample.
     */
    private convertItemFromServerWithZygosity(sample: SampleWithVariantZygosity): SampleWithVariantZygosity {
        const copy: SampleWithVariantZygosity = Object.assign({}, sample);
        copy.birthDate = this.dateUtils
            .convertLocalDateFromServer(sample.birthDate);
        copy.deathDate = this.dateUtils
            .convertLocalDateFromServer(sample.deathDate);
        return copy;
    }

    /**
     * Convert a returned JSON object to Sample.
     */
    private convertItemFromServer(sample: Sample): Sample {
        const copy: Sample = Object.assign({}, sample);
        copy.birthDate = this.dateUtils
            .convertLocalDateFromServer(sample.birthDate);
        copy.deathDate = this.dateUtils
            .convertLocalDateFromServer(sample.deathDate);
        return copy;
    }

    /**
     * Convert a Sample to a JSON which can be sent to the server.
     */
    private convert(sample: Sample): Sample {
        const copy: Sample = Object.assign({}, sample);
        copy.birthDate = this.dateUtils
            .convertLocalDateToServer(sample.birthDate);
        copy.deathDate = this.dateUtils
            .convertLocalDateToServer(sample.deathDate);
        return copy;
    }
}
