import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { Sample } from './sample.model';
import { SampleService } from './sample.service';

@Injectable()
export class SamplePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private sampleService: SampleService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.sampleService.find(id)
                    .subscribe((sampleResponse: HttpResponse<Sample>) => {
                        const sample: Sample = sampleResponse.body;
                        if (sample.birthDate) {
                            sample.birthDate = {
                                year: sample.birthDate.getFullYear(),
                                month: sample.birthDate.getMonth() + 1,
                                day: sample.birthDate.getDate()
                            };
                        }
                        if (sample.deathDate) {
                            sample.deathDate = {
                                year: sample.deathDate.getFullYear(),
                                month: sample.deathDate.getMonth() + 1,
                                day: sample.deathDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.sampleModalRef(component, sample);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.sampleModalRef(component, new Sample());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    sampleModalRef(component: Component, sample: Sample): NgbModalRef {
        const modalRef = this.modalService.open(component, { 
            // size: 'lg',
            backdrop: 'static'
        });
        modalRef.componentInstance.sample = sample;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
