import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'shrink-column',
    templateUrl: './shrink-column.component.html'
})
export class ShrinkColumnComponent {

    @Input('text') text: string; 
    @Input('width') width: number; 
    @Input('tail') tail: string; 

    public isLong: boolean;

    constructor(

    ){}

    ngOnChanges(){
        if(!this.text){
        }else if(this.text.length < this.width) {
            this.isLong = false;
        } else {
            this.isLong = true;
        }
    }
}
