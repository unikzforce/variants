import { Province } from '../enums/province.enum';
import { Injectable } from '@angular/core';

@Injectable()
export class ProvinceService {

    getProvinceItemList() : any[] {
        let items = [];
        let i =0;
        for (let province in Province) {
            let item = {}
            if (!isNaN(Number(province))) {
                item['id'] = province;
            }else{
                items[i++]['itemName'] = province;
            }
            items.push(item);
        }
        return items;
    }

}