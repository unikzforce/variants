import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

import {
    VariantsSharedLibsModule,
    VariantsSharedCommonModule,
    CSRFService,
    AuthServerProvider,
    AccountService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    JhiLoginModalComponent,
    Principal,
    HasAnyAuthorityDirective,
    ShrinkColumnComponent,
} from './';

import { ProvinceService } from './enum-services/province.service';

@NgModule({
    imports: [
        VariantsSharedLibsModule,
        VariantsSharedCommonModule
    ],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        ShrinkColumnComponent
    ],
    providers: [
        LoginService,
        LoginModalService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        AuthServerProvider,
        UserService,
        DatePipe,
        ProvinceService
    ],
    entryComponents: [JhiLoginModalComponent],
    exports: [
        VariantsSharedCommonModule,
        JhiLoginModalComponent,
        ShrinkColumnComponent,
        HasAnyAuthorityDirective,
        DatePipe,
        // ProvinceService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class VariantsSharedModule {}
