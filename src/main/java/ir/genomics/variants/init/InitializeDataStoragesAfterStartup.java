package ir.genomics.variants.init;

import java.io.IOException;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import ir.genomics.variants.repository.VariantSampleRelationInDiskRepository;
import ir.genomics.variants.repository.VariantSampleRelationInMemoryRepository;
import ir.genomics.variants.repository.variant.VariantInMemoryRepository;

@Component
public class InitializeDataStoragesAfterStartup {
	
//	private final VariantInMemoryRepository variantInMemoryRepository;

	private final VariantSampleRelationInDiskRepository vsrInDiskRepository;
	private final VariantSampleRelationInMemoryRepository vsrInMemoryRepository;
	
	public InitializeDataStoragesAfterStartup(VariantSampleRelationInDiskRepository vsrInDiskRepository,
			VariantSampleRelationInMemoryRepository vsrInMemoryRepository) {
		this.vsrInDiskRepository = vsrInDiskRepository;
		this.vsrInMemoryRepository = vsrInMemoryRepository;
	}
	
	
	// After Application get Initialized or refreshed
	@EventListener
	public void onApplicationEvent(ContextRefreshedEvent event) throws IOException {
		vsrInDiskRepository.initializeHBaseTables();
//		variantInMemoryRepository.loadAllVariantsIntoMemory();
		vsrInMemoryRepository.loadAllVsrIntoMemory();
	}

}
