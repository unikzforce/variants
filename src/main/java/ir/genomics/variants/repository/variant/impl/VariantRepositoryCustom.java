package ir.genomics.variants.repository.variant.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ir.genomics.variants.domain.Variant;

public interface VariantRepositoryCustom {
	
	Page<Variant> findByStartPositionAndEndPosition(Pageable pageable, Long startPosition, Long endPosition);

}
