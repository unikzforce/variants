package ir.genomics.variants.repository.variant;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ir.genomics.variants.domain.VariantMini;

@Repository
public interface VariantMiniRepository extends MongoRepository<VariantMini, String> {

}
