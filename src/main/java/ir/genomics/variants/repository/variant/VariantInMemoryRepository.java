package ir.genomics.variants.repository.variant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import ir.genomics.variants.domain.VariantMini;
import ir.genomics.variants.service.dto.VariantSearchResultDTO;

@Repository
public class VariantInMemoryRepository {
	
	private final Logger logger = LoggerFactory.getLogger(VariantInMemoryRepository.class); 
	
	private final VariantMiniRepository variantMiniRepository;
	private IMap<ObjectId, VariantMini> variantsMiniMap;
	private final HazelcastInstance hazelcastInstance;
	
	public VariantInMemoryRepository(HazelcastInstance hazelcastInstance, VariantMiniRepository variantMiniRepository) {
		this.variantMiniRepository = variantMiniRepository;
		this.hazelcastInstance = hazelcastInstance;
	}
	
	@PostConstruct
	public void init() {
		this.variantsMiniMap = hazelcastInstance.getMap("variantsMiniMap");
		this.variantsMiniMap.addIndex("position", true);
	}
	
	public void loadAllVariantsIntoMemory() {
		if(variantMiniRepository.count() <= 0) {
			logger.info("No data exists to import in Variants map");
			return;
		}
		
		logger.info("Cleaning Variants map");
		variantsMiniMap.clear();
		logger.info("Filling Variants map STARTED");
		final int pageLimit = 3000;
		int pageNumber = 0;
        Page<VariantMini> page = variantMiniRepository.findAll(new PageRequest(pageNumber, pageLimit));
        while (page.hasNext()) {
        	Map<ObjectId, VariantMini> container = new HashMap<>(page.getSize());
        	page.getContent().forEach(variantMiniItem -> {
        		container.put(new ObjectId(variantMiniItem.getId()), variantMiniItem);
            });
            variantsMiniMap.putAll(container);
            page = variantMiniRepository.findAll(new PageRequest(++pageNumber, pageLimit));
        }
        // process last page
        Map<ObjectId, VariantMini> container = new HashMap<>(page.getSize());
    	page.getContent().forEach(variantMiniItem -> {
        	container.put(new ObjectId(variantMiniItem.getId()), variantMiniItem);
        });
    	variantsMiniMap.putAll(container);
		logger.info("Filling Variants map FINISHED");
	}
	
	public void addAll(Map<ObjectId, VariantMini> newVariants) {
		variantsMiniMap.putAll(newVariants);
	}

}
