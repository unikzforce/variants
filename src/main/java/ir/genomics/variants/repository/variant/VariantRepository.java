package ir.genomics.variants.repository.variant;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import ir.genomics.variants.domain.Variant;
import ir.genomics.variants.repository.variant.impl.VariantRepositoryCustom;

@Repository
public interface VariantRepository extends MongoRepository<Variant, String>, VariantRepositoryCustom, QueryDslPredicateExecutor<Variant>{

	Page<Variant> findByRefGene(String refGene, Pageable pageable );
	
	Variant findOneByPositionAndAlt(Long position, String alt);
	
	Variant findOneBySnp142(String snp142);
	
}
