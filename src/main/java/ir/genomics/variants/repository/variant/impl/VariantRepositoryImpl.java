package ir.genomics.variants.repository.variant.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Predicate;

import ir.genomics.variants.domain.QVariant;
import ir.genomics.variants.domain.Variant;
import ir.genomics.variants.repository.variant.VariantRepository;

public class VariantRepositoryImpl implements VariantRepositoryCustom{
	
	@Autowired
	private VariantRepository variantRepository;
	
	@Override
	public Page<Variant> findByStartPositionAndEndPosition(Pageable pageable, Long startPosition, Long endPosition) {
		QVariant variant = QVariant.variant;
		Predicate predicate = variant.position.goe(startPosition).and(variant.position.loe(endPosition));
		return variantRepository.findAll(predicate, pageable);
	}

}