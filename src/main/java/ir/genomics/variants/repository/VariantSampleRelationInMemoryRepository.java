package ir.genomics.variants.repository;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import com.hazelcast.aggregation.Aggregator;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import com.hazelcast.query.SqlPredicate;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;

import ir.genomics.variants.domain.QDisease;
import ir.genomics.variants.domain.QSample;
import ir.genomics.variants.domain.enumeration.Zygosity;
import ir.genomics.variants.repository.exception.CorruptVariantSampleRelationException;
import ir.genomics.variants.repository.sample.SampleRepository;
import ir.genomics.variants.repository.util.AlleleFrequencyNumerationAggregator;
import ir.genomics.variants.service.dto.VariantSampleDiseasePrevalenceDTO;
import ir.genomics.variants.service.dto.VariantSearchResultDTO;

@Repository
public class VariantSampleRelationInMemoryRepository {

	private Logger logger = LoggerFactory.getLogger(VariantSampleRelationInMemoryRepository.class);

	private IMap<String, Zygosity> vsrMap;
	private final VariantSampleRelationInDiskRepository vsrInDiskRepository;
	private final HazelcastInstance hazelcastInstance;
	private final SampleRepository sampleRepository;

	public VariantSampleRelationInMemoryRepository(HazelcastInstance hazelcastInstance,
			VariantSampleRelationInDiskRepository vsrInDiskRepository, SampleRepository sampleRepository) {
		this.vsrInDiskRepository = vsrInDiskRepository;
		this.hazelcastInstance = hazelcastInstance;
		this.sampleRepository = sampleRepository;
	}

	@PostConstruct
	public void init() {
		this.vsrMap = this.hazelcastInstance.getMap("vsrMap");
	}

	public void loadAllVsrIntoMemory() throws IOException {
		final long SAMPLE_COUNT = sampleRepository.count();

		if (SAMPLE_COUNT <= 0) {
			logger.info("No data exists to import in VariantSampleRelation map");
			return;
		}

		logger.info("Cleaning VariantSampleRelation map");
		vsrMap.clear();
		logger.info("Filling VariantSampleRelation map STARTED");
		final int PAGE_SIZE = 300;
		final int PAGES = (int) (SAMPLE_COUNT / PAGE_SIZE);
		final int REMAINDER = (int) (SAMPLE_COUNT % PAGE_SIZE);

		Table table = vsrInDiskRepository.openVsrTable();

		for (int i = 0; i < PAGES; i++)
			for (Long id : sampleRepository.getSampleIds(i, PAGE_SIZE))
				loadSampleIntoVsrMap(table, id);
		for (Long id : sampleRepository.getSampleIds(PAGES, REMAINDER))
			loadSampleIntoVsrMap(table, id);
		logger.info("Filling VariantSampleRelation map FINISHED");
		table.close();
	}

	public void addAll(Map<String, Zygosity> newItems) {
		this.vsrMap.putAll(newItems);
	}

	public void loadSampleIntoVsrMap(Table table, Long sampleId) throws IOException {
		Scan scan = new Scan();
		// scan.setRaw

		// Set Client Caching TODO TODO TODO
		// scan.setCaching(10);

		final byte[] columnByteArray = Bytes.toBytes(sampleId);

		scan.addColumn(VariantSampleRelationInDiskRepository.VSR_COLUMN_FAMILY_BYTE_ARRAY, columnByteArray);

		ResultScanner scanner = table.getScanner(scan);

		for (Result result = scanner.next(); result != null; result = scanner.next())
			vsrMap.put(sampleId + Bytes.toString(result.getRow()), Zygosity.fromInt(Bytes.toInt(result.value())));

		scanner.close();
	}

	public void countAndFillPercentage(VariantSearchResultDTO variantSearchResultDTO, List<Long> sampleIds) {
		String[] keys = new String[sampleIds.size()];
		// Set<String> keys = new HashSet<String>(sampleIds.size());
		// sampleIds.forEach( id -> keys.add(id + variantSearchResultDTO.getId()));
		for (int i = 0; i < sampleIds.size(); i++)
			keys[i] = sampleIds.get(i) + variantSearchResultDTO.getId();

		int denominator = 2 * sampleIds.size();
		// int numerator = 0;
		// // without aggregation:
		// Map<String, Zygosity> results = this.vsrMap.getAll(keys);
		// for (Map.Entry<String, Zygosity> item : results.entrySet()) {
		// if (item.getValue() == null)
		// continue;
		//
		// // TODO NULLYZIGOUS
		// switch (item.getValue()) {
		// case HETEROZYGOUS:
		// case HET_COM:
		// numerator++;
		// continue;
		// case HOMOZYGOUS:
		// numerator += 2;
		// continue;
		// case HEMIZYGOUS:
		// numerator++;
		// continue;
		// }
		//
		// }

		Aggregator<Map.Entry<String, Zygosity>, Integer> numeratorAggregator = new AlleleFrequencyNumerationAggregator();

		// TODO do it with aggregation
		EntryObject entryObject = new PredicateBuilder().getEntryObject();
		Predicate<String, Zygosity> pred = entryObject.key().in(keys);

		int numerator = this.vsrMap.aggregate(numeratorAggregator, pred);

		variantSearchResultDTO.setAlleleFrequency(((double) numerator) / denominator);

	}

	public void countAndFillPercentage(Page<VariantSearchResultDTO> variantSearchResultDTOPage, List<Long> sampleIds) {
		for (VariantSearchResultDTO dto : variantSearchResultDTOPage)
			this.countAndFillPercentage(dto, sampleIds);
	}

	public VariantSampleDiseasePrevalenceDTO getVariantExtra(String variantId) throws CorruptVariantSampleRelationException {
		
		VariantSampleDiseasePrevalenceDTO result = new VariantSampleDiseasePrevalenceDTO();
		result.setId(variantId);
		Set<Entry<String, Zygosity>> items = this.vsrMap
				.entrySet(new SqlPredicate("__key REGEX '(\\d+)" + variantId + "'"));

		List<Long> sampleIds = new ArrayList<Long>();

		for (Entry<String, Zygosity> item : items) {
			
			if (item.getKey().length() <= 24)
				throw new CorruptVariantSampleRelationException(
						"A <sampleId + variantId> found with length <= 24 : " + item.getKey());
			
			sampleIds.add(Long.parseLong(item.getKey().substring(0, (item.getKey().length() - 24))));
		}

		result.setSampleCount(sampleIds.size());
		result.setPrevalences(sampleRepository.findPrevalenceFor(sampleIds));
		
		return result;
	}
	
	public IMap<String, Zygosity> getMap() {
		return this.vsrMap;
	}

}
