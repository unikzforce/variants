package ir.genomics.variants.repository.exception;

public class CorruptVariantSampleRelationException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public String message;

    public CorruptVariantSampleRelationException (String message){
        this.message = message;
    }

    // Overrides Exception's getMessage()
    @Override
    public String getMessage(){
        return message;
    }
}
