package ir.genomics.variants.repository;

import org.springframework.stereotype.Repository;

import ir.genomics.variants.domain.Race;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Race entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RaceRepository extends JpaRepository<Race, Long> {

}
