package ir.genomics.variants.repository.sample;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import ir.genomics.variants.domain.Sample;
import ir.genomics.variants.repository.sample.impl.SampleRepositoryCustom;

@Repository
public interface SampleRepository extends JpaRepository<Sample, Long>, SampleRepositoryCustom, QueryDslPredicateExecutor<Sample>{
	List<Sample> findByDiseasesId(Long diseaseId);
}
