package ir.genomics.variants.repository.sample.impl;


import java.util.List;

import ir.genomics.variants.domain.enumeration.Gender;
import ir.genomics.variants.domain.enumeration.Province;
import ir.genomics.variants.service.dto.DiseasePrevalenceDTO;

public interface SampleRepositoryCustom {
	List<Long> getSampleIds(int pageNumber, int pageSize);
	
	List<Long> searchAllSamples(Integer ageStart, Integer ageEnd, Gender[] genders, Province[] birthProvince, Long[] diseaseIds, Long[] raceIds);
	
	List<DiseasePrevalenceDTO> findPrevalenceFor(List<Long> sampleIds);
	
}
