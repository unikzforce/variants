package ir.genomics.variants.repository.sample.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;

import ir.genomics.variants.domain.Disease;
import ir.genomics.variants.domain.QDisease;
import ir.genomics.variants.domain.QSample;
import ir.genomics.variants.domain.Sample;
import ir.genomics.variants.domain.enumeration.Gender;
import ir.genomics.variants.domain.enumeration.Province;
import ir.genomics.variants.repository.sample.SampleRepository;
import ir.genomics.variants.service.dto.DiseasePrevalenceDTO;

public class SampleRepositoryImpl implements SampleRepositoryCustom {

	@Autowired
	private SampleRepository sampleRepository;

	private final EntityManager entityManager;

	public SampleRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public List<Long> getSampleIds(int pageNumber, int pageSize) {
		return new JPAQuery<Long>(entityManager).select(QSample.sample.id).from(QSample.sample)
				.offset(pageNumber * pageSize).limit(pageSize).fetch();
	}

	@Override
	public List<Long> searchAllSamples(Integer ageStart, Integer ageEnd, Gender[] genders, Province[] birthProvinces,
			Long[] diseaseIds, Long[] raceIds) {
		QSample sample = QSample.sample;
		JPAQuery<Long> query = new JPAQuery<Long>(entityManager).select(sample.id).from(sample);

		BooleanBuilder booleanPredicateBuilder = new BooleanBuilder();

		if (ageStart != null)
			booleanPredicateBuilder.and(sample.age.goe(ageStart));

		if (ageEnd != null)
			booleanPredicateBuilder.and(sample.age.loe(ageEnd));

		if (!ArrayUtils.isEmpty(genders))
			booleanPredicateBuilder.and(sample.gender.in(genders));

		if (!ArrayUtils.isEmpty(birthProvinces))
			booleanPredicateBuilder.and(sample.birthProvince.in(birthProvinces));

		if (!ArrayUtils.isEmpty(diseaseIds))
			booleanPredicateBuilder.and(sample.diseases.any().id.in(Arrays.asList(diseaseIds)));

		if (!ArrayUtils.isEmpty(raceIds))
			booleanPredicateBuilder.and(sample.diseases.any().id.in(Arrays.asList(raceIds)));

		return query.where(booleanPredicateBuilder).fetch();
	}

//	@Override
//	public List<DiseasePrevalenceDTO> findPrevalenceFor(List<Long> sampleIds) {
//		QSample sample = QSample.sample;
//		QDisease disease = QDisease.disease;
////		JPAQuery subQuery = new JPAQuery<>(entityManager).from(sample).innerJoin(sample.diseases).join(disease)
////				.groupBy(disease.id);
//		JPQLQuery<Tuple> q = new JPAQuery<>(entityManager).select(disease.id, sample.count()).from(sample)
//				.rightJoin(sample.diseases).join(disease).where(sample.id.(sampleIds));
//		QueryResults<Tuple> queryResults = q.fetchResults();
//
//		List<DiseasePrevalenceDTO> results = new ArrayList<DiseasePrevalenceDTO>((int) queryResults.getTotal());
//		DiseasePrevalenceDTO item = null;
//		
//		for (Tuple t: queryResults.getResults())
//			System.out.println("YESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS" + t);
//
////		for (Tuple t : queryResults.getResults()) {
////			item = new DiseasePrevalenceDTO();
////			item.setId(t.get(disease.id));
////			item.setName(t.get(disease.name));
////			item.setPrevalence(t.get(sample.count()).intValue());
////		}
//		return results;
//	}
	
	@Override
	public List<DiseasePrevalenceDTO> findPrevalenceFor(List<Long> sampleIds) {
		QSample sample = QSample.sample;
		QDisease disease = QDisease.disease;
//		JPAQuery subQuery = new JPAQuery<>(entityManager).from(sample).innerJoin(sample.diseases).join(disease)
//				.groupBy(disease.id);
//		JPQLQuery<Tuple> q = new JPAQuery<>(entityManager).select(disease.id, sample.count()).from(sample)
//				.rightJoin(sample.diseases).join(disease).where(sample.id.(sampleIds));
		JPQLQuery<Tuple> q = new JPAQuery<>(entityManager).select(disease.id, disease.name, sample.countDistinct())
				.groupBy(disease.id).from(sample, disease).join(sample.diseases, disease).where(sample.id.in(sampleIds));
		QueryResults<Tuple> queryResults = q.fetchResults();

		List<DiseasePrevalenceDTO> results = new ArrayList<DiseasePrevalenceDTO>((int) queryResults.getTotal());
		DiseasePrevalenceDTO item = null;
		
		for (Tuple t : queryResults.getResults()) {
			item = new DiseasePrevalenceDTO();
			item.setId(t.get(disease.id));
			item.setName(t.get(disease.name));
			item.setPrevalence(t.get(sample.countDistinct()).intValue());
			results.add(item);
		}
		return results;
	}
	
//	public List<Sample> findAllByDiseaseId(Long diseaseId) {
//		QSample sample = QSample.sample;
//		sample.diseases.id
//	}
	
	
//	// rewritten
//	@Override
//	public List<DiseasePrevalenceDTO> findPrevalenceFor(List<Long> sampleIds) {
//		Set<Disease> allDiseases = new HashSet<Disease>();
//		QSample sample = QSample.sample;
//		for(Long sampleId: sampleIds) {
//			Set<Disease> diseases = sampleRepository.getOne(sampleId).getDiseases();
//			if( !CollectionUtils.isEmpty(diseases) )
//				allDiseases.addAll(diseases);
//		}
//		for(Disease d: allDiseases) {
//			JPAQuery<Long> q = new JPAQuery<Long>(entityManager).from.select(sample.count()).where(sample.diseases.contains(d));
//			
//			
//		}
//		return null;
//	}

}
