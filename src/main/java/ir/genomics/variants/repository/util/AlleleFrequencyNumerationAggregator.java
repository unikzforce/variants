package ir.genomics.variants.repository.util;

import java.util.Map;
import java.util.Map.Entry;

import com.hazelcast.aggregation.Aggregator;

import ir.genomics.variants.domain.enumeration.Zygosity;

public class AlleleFrequencyNumerationAggregator extends  Aggregator<Map.Entry<String,Zygosity>, Integer> {

	protected int numerator = 0;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void accumulate(Entry<String, Zygosity> input) {
		if(input.getValue() == null)
			return;
		// TODO NULLYZIGOUS
		switch (input.getValue()) {
		case HETEROZYGOUS:
		case HET_COM:
			numerator++;
			return;
		case HOMOZYGOUS:
			numerator += 2;
			return;
		case HEMIZYGOUS:
			numerator++;
			return;
		}
	}

	@Override
	public void combine(Aggregator aggregator) {
		this.numerator += this.getClass().cast(aggregator).numerator;
	}

	@Override
	public Integer aggregate() {
		return this.numerator;
	}
};