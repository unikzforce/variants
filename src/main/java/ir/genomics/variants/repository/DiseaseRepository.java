package ir.genomics.variants.repository;

import org.springframework.stereotype.Repository;

import ir.genomics.variants.domain.Disease;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Disease entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DiseaseRepository extends JpaRepository<Disease, Long> {

}
