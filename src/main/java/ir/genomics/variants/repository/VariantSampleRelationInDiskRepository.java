package ir.genomics.variants.repository;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.stereotype.Repository;

@Repository
public class VariantSampleRelationInDiskRepository {
	
	private final Connection hbaseConnection;
	
	
	public static final TableName VSR_TABLE_NAME = TableName.valueOf("variant_sample_relations");
	public static final String VSR_COLUMN_FAMILY = "vsr_family_1";
	public static final byte[] VSR_COLUMN_FAMILY_BYTE_ARRAY = Bytes.toBytes(VSR_COLUMN_FAMILY);
	
	private Table vsrTable = null;
	
	public VariantSampleRelationInDiskRepository(Connection hbaseConnection) throws IOException {
		
		this.hbaseConnection = hbaseConnection;
		
//		this.initializeHBaseTables();					// TODO change place for THIS ONE?
		
//		Scan scan = new Scan();
//		scan.addColumn(VSR_COLUMN_FAMILY_BYTE_ARRAY, Bytes.toBytes(1l));
//		vsrTable = hbaseConnection.getTable(VSR_TABLE_NAME);
		
//		Put put = new Put(Bytes.toBytes("variant_1"));
//		put.addColumn(VSR_COLUMN_FAMILY_BYTE_ARRAY, Bytes.toBytes("column_1"), Bytes.toBytes("Hellowwww Hbase"));
//		
//		vsrTable.put(put);
//		
//		Get get = new Get(Bytes.toBytes("variant_1"));
//		get.addColumn(VSR_COLUMN_FAMILY_BYTE_ARRAY, Bytes.toBytes("column_1"));
//		Result res = vsrTable.get(get);
		
//		List<Cell> cells =;
//		for(Cell c: res.listCells())
			
//		System.out.println("REEEEEEEEEEEEEEESUUUUUUUUUUUUUUUUULTTTTTTTTTTTTTTTTT" + Bytes.toString(res.getValue(VSR_COLUMN_FAMILY_BYTE_ARRAY, Bytes.toBytes("column_1"))));
//		vsrTable.close();
		
//		hbaseTemplate.put("test_table", "test_row_1", "test_family", "test_column_qualifier", Bytes.toBytes("Hellowwww Hbase"));
//		RowMapper<String>  strMapper = new RowMapper<String>() {
//			@Override
//			public String mapRow(Result result, int rowNum) throws Exception {
//				return Bytes.toString(result.value());
//			}
//		};
//		String res = hbaseTemplate.get("test_table", "test_row_1", "test_family", "test_column_qualifier",strMapper);
//		System.out.println("REEEEEEEEEEEEESUUUUUUUUUUUUUUULTTTTTTTT: " + res);
	}

	public Admin openAdmin() throws IOException {
		return this.hbaseConnection.getAdmin();
	}
	
	public Table openVsrTable() throws IOException {
		this.vsrTable = hbaseConnection.getTable(VSR_TABLE_NAME);
		return this.vsrTable;
	}
	
	public void putInVsrTable(List<Put> putList) throws IOException {
		this.vsrTable = hbaseConnection.getTable(VSR_TABLE_NAME);
		this.vsrTable.put(putList);
		this.vsrTable.close();
	}
	
	public void initializeHBaseTables() throws IOException {
		hbaseConnection.getTable(VSR_TABLE_NAME);
		Admin admin = hbaseConnection.getAdmin();
		
		if( !admin.tableExists(VSR_TABLE_NAME) ) {
			HTableDescriptor desc = new HTableDescriptor(VSR_TABLE_NAME);
			desc.addFamily( new HColumnDescriptor(VSR_COLUMN_FAMILY));
			admin.createTable(desc);
		}
		
		admin.close();
	}
	
}
