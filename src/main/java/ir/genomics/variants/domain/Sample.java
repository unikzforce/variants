package ir.genomics.variants.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import ir.genomics.variants.domain.enumeration.Gender;

import ir.genomics.variants.domain.enumeration.Province;

import ir.genomics.variants.domain.enumeration.SampleImportingStatus;

/**
 * A Sample.
 */
@Entity
@Table(name = "sample")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Sample implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "file_original_name")
    private String fileOriginalName;

    @Column(name = "file_hash_name")
    private String fileHashName;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "age")
    private Integer age;

    @Column(name = "national_code")
    private String nationalCode;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Column(name = "death_date")
    private LocalDate deathDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Enumerated(EnumType.STRING)
    @Column(name = "birth_province")
    private Province birthProvince;

    @Enumerated(EnumType.STRING)
    @Column(name = "living_province")
    private Province livingProvince;

    @Enumerated(EnumType.STRING)
    @Column(name = "importing_status")
    private SampleImportingStatus importingStatus;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "sample_races",
               joinColumns = @JoinColumn(name="samples_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="races_id", referencedColumnName="id"))
    private Set<Race> races = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "sample_diseases",
               joinColumns = @JoinColumn(name="samples_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="diseases_id", referencedColumnName="id"))
    private Set<Disease> diseases = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileOriginalName() {
        return fileOriginalName;
    }

    public Sample fileOriginalName(String fileOriginalName) {
        this.fileOriginalName = fileOriginalName;
        return this;
    }

    public void setFileOriginalName(String fileOriginalName) {
        this.fileOriginalName = fileOriginalName;
    }

    public String getFileHashName() {
        return fileHashName;
    }

    public Sample fileHashName(String fileHashName) {
        this.fileHashName = fileHashName;
        return this;
    }

    public void setFileHashName(String fileHashName) {
        this.fileHashName = fileHashName;
    }

    public String getFirstName() {
        return firstName;
    }

    public Sample firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Sample lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public Sample age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public Sample nationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
        return this;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Sample birthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDate getDeathDate() {
        return deathDate;
    }

    public Sample deathDate(LocalDate deathDate) {
        this.deathDate = deathDate;
        return this;
    }

    public void setDeathDate(LocalDate deathDate) {
        this.deathDate = deathDate;
    }

    public Gender getGender() {
        return gender;
    }

    public Sample gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Province getBirthProvince() {
        return birthProvince;
    }

    public Sample birthProvince(Province birthProvince) {
        this.birthProvince = birthProvince;
        return this;
    }

    public void setBirthProvince(Province birthProvince) {
        this.birthProvince = birthProvince;
    }

    public Province getLivingProvince() {
        return livingProvince;
    }

    public Sample livingProvince(Province livingProvince) {
        this.livingProvince = livingProvince;
        return this;
    }

    public void setLivingProvince(Province livingProvince) {
        this.livingProvince = livingProvince;
    }

    public SampleImportingStatus getImportingStatus() {
        return importingStatus;
    }

    public Sample importingStatus(SampleImportingStatus importingStatus) {
        this.importingStatus = importingStatus;
        return this;
    }

    public void setImportingStatus(SampleImportingStatus importingStatus) {
        this.importingStatus = importingStatus;
    }

    public Set<Race> getRaces() {
        return races;
    }

    public Sample races(Set<Race> races) {
        this.races = races;
        return this;
    }

//    public Sample addRaces(Race race) {
//        this.races.add(race);
//        race.getSamples().add(this);
//        return this;
//    }
//
//    public Sample removeRaces(Race race) {
//        this.races.remove(race);
//        race.getSamples().remove(this);
//        return this;
//    }

    public void setRaces(Set<Race> races) {
        this.races = races;
    }

    public Set<Disease> getDiseases() {
        return diseases;
    }

    public Sample diseases(Set<Disease> diseases) {
        this.diseases = diseases;
        return this;
    }

//    public Sample addDiseases(Disease disease) {
//        this.diseases.add(disease);
//        disease.getSamples().add(this);
//        return this;
//    }
//
//    public Sample removeDiseases(Disease disease) {
//        this.diseases.remove(disease);
//        disease.getSamples().remove(this);
//        return this;
//    }

    public void setDiseases(Set<Disease> diseases) {
        this.diseases = diseases;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sample sample = (Sample) o;
        if (sample.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sample.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Sample{" +
            "id=" + getId() +
            ", fileOriginalName='" + getFileOriginalName() + "'" +
            ", fileHashName='" + getFileHashName() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", age=" + getAge() +
            ", nationalCode='" + getNationalCode() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", deathDate='" + getDeathDate() + "'" +
            ", gender='" + getGender() + "'" +
            ", birthProvince='" + getBirthProvince() + "'" +
            ", livingProvince='" + getLivingProvince() + "'" +
            ", importingStatus='" + getImportingStatus() + "'" +
            "}";
    }
}
