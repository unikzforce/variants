package ir.genomics.variants.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="variants")
public class VariantMini {
	
	private String id;
	
	private long position;
	
	private String alt;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getPosition() {
		return position;
	}

	public void setPosition(long position) {
		this.position = position;
	}
	
	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}
}
