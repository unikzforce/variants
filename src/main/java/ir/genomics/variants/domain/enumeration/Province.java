package ir.genomics.variants.domain.enumeration;

/**
 * The Province enumeration.
 */
public enum Province {
	ALBORZ(1), ARDABIL(2), AZERBAIJAN_EAST(3), AZERBAIJAN_WEST(4), BUSHEHR(5), CHAHAR_MAHAL_AND_BAKHTIARI(6), FARS(
			7), GILAN(8), GOLESTAN(9), HAMADAN(10), HORMOZGAN(11), ILAM(12), ISFAHAN(13), KERMAN(14), KERMANSHAH(
					15), KHORASAN_NORTH(16), KHORASAN_RAZAVI(17), KHORASAN_SOUTH(18), KHUZESTAN(
							19), KOHGILUYEH_AND_BOYER_AHMAD(20), KURDESTAN(21), LORESTAN(22), MARKAZI(23), MAZANDARAN(
									24), QAZVIN(25), QOM(26), SEMNAN(
											27), SISTAN_AND_BALUCHESTAN(28), TEHRAN(29), YAZD(30), ZANJAN(31);

	private final int value;

	private Province(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static Province parse(int val) {
		switch (val) {
		case 1:
			return Province.ALBORZ;
		case 2:
			return Province.ARDABIL;
		case 3:
			return Province.AZERBAIJAN_EAST;
		case 4:
			return Province.AZERBAIJAN_WEST;
		case 5:
			return Province.BUSHEHR;
		case 6:
			return Province.CHAHAR_MAHAL_AND_BAKHTIARI;
		case 7:
			return Province.FARS;
		case 8:
			return Province.GILAN;
		case 9:
			return Province.GOLESTAN;
		case 10:
			return Province.HAMADAN;
		case 11:
			return Province.HORMOZGAN;
		case 12:
			return Province.ILAM;
		case 13:
			return Province.ISFAHAN;
		case 14:
			return Province.KERMAN;
		case 15:
			return Province.KERMANSHAH;
		case 16:
			return Province.KHORASAN_NORTH;
		case 17:
			return Province.KHORASAN_RAZAVI;
		case 18:
			return Province.KHORASAN_SOUTH;
		case 19:
			return Province.KHUZESTAN;
		case 20:
			return Province.KOHGILUYEH_AND_BOYER_AHMAD;
		case 21:
			return Province.KURDESTAN;
		case 22:
			return Province.LORESTAN;
		case 23:
			return Province.MARKAZI;
		case 24:
			return Province.MAZANDARAN;
		case 25:
			return Province.QAZVIN;
		case 26:
			return Province.QOM;
		case 27:
			return Province.SEMNAN;
		case 28:
			return Province.SISTAN_AND_BALUCHESTAN;
		case 29:
			return Province.TEHRAN;
		case 30:
			return Province.YAZD;
		case 31:
			return Province.ZANJAN;
		default:
			return null;
		}
	}
	
	public static Province[] parse(Integer[] vals) {
		Province[] results = new Province[vals.length];
		for(int i=0; i<vals.length; i++)
			results[i] = Province.parse(vals[i]);
		return results;
	}
	
	public static Province[] valueOf(String[] vals) {
		Province[] results = new Province[vals.length];
		for(int i=0; i<vals.length; i++)
			results[i] = Province.valueOf(vals[i]);
		return results;
	}
}
