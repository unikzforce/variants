package ir.genomics.variants.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE(1),  FEMALE(2),  OTHER(3);
    
	private final int value;
	
	private Gender(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public static Gender parse(int val) {
		if(val == 1)
			return Gender.MALE;
		else if(val == 2)
			return Gender.FEMALE;
		else if(val == 3)
			return Gender.OTHER;
		return null;
	}
	
	public static Gender[] parse(Integer[] vals) {
		Gender[] result = new Gender[vals.length];
		for(int i=0; i<vals.length; i++)
			result[i] = Gender.parse(vals[i]);
		return result;
	}
	
	public static Gender[] valueOf(String[] vals) {
		Gender[] result = new Gender[vals.length];
		for(int i=0; i<vals.length; i++)
			result[i] = Gender.valueOf(vals[i]);
		return result;
	}
	
}
