package ir.genomics.variants.domain.enumeration;

public enum Chromosome {
	CHR_1(1),
	CHR_2(2),
	CHR_3(3),
	CHR_4(4),
	CHR_5(5),
	CHR_6(6),
	CHR_7(7),
	CHR_8(8),
	CHR_9(9),
	CHR_10(10),
	CHR_11(11),
	CHR_12(12),
	CHR_13(13),
	CHR_14(14),
	CHR_15(15),
	CHR_16(16),
	CHR_17(17),
	CHR_18(18),
	CHR_19(19),
	CHR_20(20),
	CHR_21(21),
	CHR_22(22),
	CHR_X(23),
	CHR_Y(24),
	CHR_M(25);
	
	private final int value;
	
	private Chromosome(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public static Chromosome parse(String chr) throws IllegalArgumentException {
		switch(chr) {
		case "1":
			return Chromosome.CHR_1;
		case "2":
			return Chromosome.CHR_2;
		case "3":
			return Chromosome.CHR_3;
		case "4":
			return Chromosome.CHR_4;
		case "5":
			return Chromosome.CHR_5;
		case "6":
			return Chromosome.CHR_6;
		case "7":
			return Chromosome.CHR_7;
		case "8":
			return Chromosome.CHR_8;
		case "9":
			return Chromosome.CHR_9;
		case "10":
			return Chromosome.CHR_10;
		case "11":
			return Chromosome.CHR_11;
		case "12":
			return Chromosome.CHR_12;
		case "13":
			return Chromosome.CHR_13;
		case "14":
			return Chromosome.CHR_14;
		case "15":
			return Chromosome.CHR_15;
		case "16":
			return Chromosome.CHR_16;
		case "17":
			return Chromosome.CHR_17;
		case "18":
			return Chromosome.CHR_18;
		case "19":
			return Chromosome.CHR_19;
		case "20":
			return Chromosome.CHR_20;
		case "21":
			return Chromosome.CHR_21;
		case "22":
			return Chromosome.CHR_22;
		case "X":
		case "x":
			return Chromosome.CHR_X;
		case "Y":
		case "y":
			return Chromosome.CHR_Y;
		case "M":
		case "m":
			return Chromosome.CHR_M;
		}
		throw new IllegalArgumentException("No Chromosome found with name " + chr);
	}
}
