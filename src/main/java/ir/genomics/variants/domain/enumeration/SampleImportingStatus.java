package ir.genomics.variants.domain.enumeration;

/**
 * The SampleImportingStatus enumeration.
 */
public enum SampleImportingStatus {
    NOT_STARTED(1), NOT_FINISHED(2), FINISHED(3), FAILED(4), FAILURE_CLEAN_UP(5);
	
	private final int value;
	
	private SampleImportingStatus(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
}
