package ir.genomics.variants.domain.enumeration;

public enum Zygosity {
	HOMOZYGOUS(1), HETEROZYGOUS(2), HEMIZYGOUS(3), HET_COM(4), NULLYZIGOUS(5);
	
	private final int value;
	
	private Zygosity(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public static Zygosity fromInt(int value) {
		switch(value) {
		case 1:
			return HOMOZYGOUS;
		case 2:
			return HETEROZYGOUS;
		case 3:
			return HEMIZYGOUS;
		case 4:
			return HET_COM;
		case 5:
			return NULLYZIGOUS;
		default:
			return null;
		}
	}
}
