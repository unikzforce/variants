package ir.genomics.variants.domain.enumeration;

public enum VariantType {
	INSERTION, DELETION, CHANGE
}
