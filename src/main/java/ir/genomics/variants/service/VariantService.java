package ir.genomics.variants.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ir.genomics.variants.repository.exception.CorruptVariantSampleRelationException;
import ir.genomics.variants.service.dto.VariantDTO;
import ir.genomics.variants.service.dto.VariantSampleDiseasePrevalenceDTO;
import ir.genomics.variants.service.dto.VariantSearchResultDTO;
import ir.genomics.variants.service.impl.exception.InvalidSearchPhraseException;

public interface VariantService {
	
	public Page<VariantSearchResultDTO> search(Pageable pageable, String variantSearchPhrase, Integer ageStart, Integer ageEnd,
			Integer[] genders, Integer[] birthProvince, Long[] diseaseIds, Long[] raceIds) throws InvalidSearchPhraseException;
	
	public VariantDTO findOne(String id);
	
	public VariantSampleDiseasePrevalenceDTO findOnePrevalenceDTO(String id) throws CorruptVariantSampleRelationException;

}
