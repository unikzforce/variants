package ir.genomics.variants.service;

public interface VariantSampleRelationService {

	public void importDataIntoDiskDatastore();
	
	public void importDataIntoMemoryDatastore();
	
}
