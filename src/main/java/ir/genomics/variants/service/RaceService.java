package ir.genomics.variants.service;

import ir.genomics.variants.service.dto.RaceDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Race.
 */
public interface RaceService {

    /**
     * Save a race.
     *
     * @param raceDTO the entity to save
     * @return the persisted entity
     */
    RaceDTO save(RaceDTO raceDTO);

    /**
     * Get all the races.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RaceDTO> findAll(Pageable pageable);

    /**
     * Get the "id" race.
     *
     * @param id the id of the entity
     * @return the entity
     */
    RaceDTO findOne(Long id);

    /**
     * Delete the "id" race.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
