package ir.genomics.variants.service.impl.exception;

@SuppressWarnings("serial")
public class InvalidSearchPhraseException extends Exception {
    public String message;

    public InvalidSearchPhraseException (String message){
        this.message = message;
    }

    // Overrides Exception's getMessage()
    @Override
    public String getMessage(){
        return message;
    }
}
