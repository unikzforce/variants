package ir.genomics.variants.service.impl;

import org.springframework.stereotype.Service;

import ir.genomics.variants.repository.VariantSampleRelationInDiskRepository;
import ir.genomics.variants.repository.VariantSampleRelationInMemoryRepository;
import ir.genomics.variants.service.VariantSampleRelationService;

@Service
public class VariantSampleRelationServiceImpl implements VariantSampleRelationService{

	private final VariantSampleRelationInDiskRepository vsrDiskRepository;

	private final VariantSampleRelationInMemoryRepository vsrMemRepository;

	public VariantSampleRelationServiceImpl(VariantSampleRelationInDiskRepository vsrDiskRepository,
			VariantSampleRelationInMemoryRepository vsrMemRepository) {
		this.vsrDiskRepository = vsrDiskRepository;
		this.vsrMemRepository = vsrMemRepository;
	}

	@Override
	public void importDataIntoDiskDatastore() {
		// TODO Auto-generated method stub
	}

	@Override
	public void importDataIntoMemoryDatastore() {
		// TODO Auto-generated method stub
	}

}
