package ir.genomics.variants.service.impl;

import ir.genomics.variants.service.RaceService;
import ir.genomics.variants.domain.Race;
import ir.genomics.variants.repository.RaceRepository;
import ir.genomics.variants.service.dto.RaceDTO;
import ir.genomics.variants.service.mapper.RaceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Race.
 */
@Service
@Transactional
public class RaceServiceImpl implements RaceService {

    private final Logger log = LoggerFactory.getLogger(RaceServiceImpl.class);

    private final RaceRepository raceRepository;

    private final RaceMapper raceMapper;

    public RaceServiceImpl(RaceRepository raceRepository, RaceMapper raceMapper) {
        this.raceRepository = raceRepository;
        this.raceMapper = raceMapper;
    }

    /**
     * Save a race.
     *
     * @param raceDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RaceDTO save(RaceDTO raceDTO) {
        log.debug("Request to save Race : {}", raceDTO);
        Race race = raceMapper.toEntity(raceDTO);
        race = raceRepository.save(race);
        return raceMapper.toDto(race);
    }

    /**
     * Get all the races.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RaceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Races");
        return raceRepository.findAll(pageable)
            .map(raceMapper::toDto);
    }

    /**
     * Get one race by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public RaceDTO findOne(Long id) {
        log.debug("Request to get Race : {}", id);
        Race race = raceRepository.findOne(id);
        return raceMapper.toDto(race);
    }

    /**
     * Delete the race by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Race : {}", id);
        raceRepository.delete(id);
    }
}
