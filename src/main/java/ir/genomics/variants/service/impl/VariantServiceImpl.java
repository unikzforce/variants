package ir.genomics.variants.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ir.genomics.variants.domain.Variant;
import ir.genomics.variants.domain.enumeration.Chromosome;
import ir.genomics.variants.domain.enumeration.Gender;
import ir.genomics.variants.domain.enumeration.Province;
import ir.genomics.variants.repository.VariantSampleRelationInMemoryRepository;
import ir.genomics.variants.repository.exception.CorruptVariantSampleRelationException;
import ir.genomics.variants.repository.sample.SampleRepository;
import ir.genomics.variants.repository.variant.VariantRepository;
import ir.genomics.variants.service.VariantService;
import ir.genomics.variants.service.dto.VariantDTO;
import ir.genomics.variants.service.dto.VariantSampleDiseasePrevalenceDTO;
import ir.genomics.variants.service.dto.VariantSearchResultDTO;
import ir.genomics.variants.service.impl.exception.InvalidSearchPhraseException;
import ir.genomics.variants.service.mapper.VariantMapper;
import ir.genomics.variants.service.util.ChromosomePosition;
import ir.genomics.variants.service.util.VariantUtil;
import ir.genomics.variants.service.util.exception.ChromosomeRelativeToAbsolutePositionConversionException;

@Service
public class VariantServiceImpl implements VariantService {
	
    private final Logger log = LoggerFactory.getLogger(VariantServiceImpl.class);

	private final VariantRepository variantRepository;
	private final VariantSampleRelationInMemoryRepository vsrInMemoryRepository;
	private final SampleRepository sampleRepository;
	private final VariantMapper variantMapper;

	final Pattern variantSearchPattern = Pattern.compile("(\\d{1,2}|[mMxXyY]):(\\d+)-([atcgATCG]*)-([atcgATCG]*)");
	final Pattern regionSearchPattern = Pattern.compile("(\\d{1,2}|[mMxXyY]):(\\d+)-(\\d+)");
	final Pattern snp142SearchPattern = Pattern.compile("(rs\\d+)");
	final Pattern geneSearchPattern = Pattern.compile("([A-Z\\d_]+)");

	public VariantServiceImpl(VariantRepository variantRepository, VariantMapper variantMapper,
			SampleRepository sampleRepository, VariantSampleRelationInMemoryRepository vsrInMemoryRepository) {
		this.variantRepository = variantRepository;
		this.sampleRepository = sampleRepository;
		this.vsrInMemoryRepository = vsrInMemoryRepository;
		this.variantMapper = variantMapper;
	}

	@Override
	public Page<VariantSearchResultDTO> search(Pageable pageable, String variantSearchPhrase, Integer ageStart,
			Integer ageEnd, Integer[] genders, Integer[] birthProvinces, Long[] diseaseIds, Long[] raceIds) throws InvalidSearchPhraseException {
		Gender[] genderEnums = null;
		Province[] birthProvinceEnums = null;

		if ( !ArrayUtils.isEmpty(genders))
			genderEnums = Gender.parse(genders);
		if ( !ArrayUtils.isEmpty(birthProvinces))
			birthProvinceEnums = Province.parse(birthProvinces);

		List<Long> searchedSampleIds = sampleRepository.searchAllSamples(ageStart, ageEnd, genderEnums,
				birthProvinceEnums, diseaseIds, raceIds);

		Page<Variant> variantPage = null;
		Page<VariantSearchResultDTO> variantSearchResultDTOPage = null;
		Variant theVariant = null;

		Matcher geneSearchMatcher = geneSearchPattern.matcher(variantSearchPhrase);
		Matcher variantSearchMatcher = variantSearchPattern.matcher(variantSearchPhrase);
		Matcher snp142SearchMatcher = snp142SearchPattern.matcher(variantSearchPhrase);
		Matcher regionSearchMatcher = regionSearchPattern.matcher(variantSearchPhrase);

		if (variantSearchMatcher.matches()) {
			
			Chromosome chr = Chromosome.parse(variantSearchMatcher.group(1));

			try {
				theVariant = variantRepository
						.findOneByPositionAndAlt(VariantUtil.getAbsolutePosition(chr, Integer.parseInt(variantSearchMatcher.group(2))), variantSearchMatcher.group(4));
			} catch (ChromosomeRelativeToAbsolutePositionConversionException e) {
				throw new InvalidSearchPhraseException("Invalid position for chromosome " + chr);
			}
			
			if(theVariant == null)
				return new PageImpl<VariantSearchResultDTO>(new ArrayList<VariantSearchResultDTO>(), pageable, 0); 
			
			VariantSearchResultDTO resDTO = variantMapper.toVariantSearchResultDTO(theVariant);
			vsrInMemoryRepository.countAndFillPercentage(resDTO, searchedSampleIds);

			variantSearchResultDTOPage = new PageImpl<>(Arrays.asList(resDTO), pageable, 1);

		} else if (regionSearchMatcher.matches()) {
			Chromosome chr = Chromosome.parse(regionSearchMatcher.group(1));
			Long startPosition;
			try {
				startPosition = VariantUtil.getAbsolutePosition(chr, Integer.parseInt(regionSearchMatcher.group(2)));
			} catch (ChromosomeRelativeToAbsolutePositionConversionException e) {
				throw new InvalidSearchPhraseException("Invalid position for chromosomes' start range: " + chr);
			}
			Long endPosition;
			try {
				endPosition = VariantUtil.getAbsolutePosition(chr, Integer.parseInt(regionSearchMatcher.group(3)));
			} catch (ChromosomeRelativeToAbsolutePositionConversionException e) {
				throw new InvalidSearchPhraseException("Invalid position for chromosomes' end range: " + chr);
			}
			
			variantPage = variantRepository.findByStartPositionAndEndPosition(pageable, startPosition, endPosition);
			
			if(CollectionUtils.isEmpty(variantPage.getContent()))
				return new PageImpl<VariantSearchResultDTO>(new ArrayList<VariantSearchResultDTO>(), pageable, 0);			
			
			variantSearchResultDTOPage = variantPage.map(variant -> variantMapper.toVariantSearchResultDTO(variant));
			vsrInMemoryRepository.countAndFillPercentage(variantSearchResultDTOPage, searchedSampleIds);

		} else if (geneSearchMatcher.matches()) {
			variantPage = variantRepository.findByRefGene(variantSearchPhrase, pageable);

			if(CollectionUtils.isEmpty(variantPage.getContent()))
				return new PageImpl<VariantSearchResultDTO>(new ArrayList<VariantSearchResultDTO>(), pageable, 0);			
			
			variantSearchResultDTOPage = variantPage.map(variant -> variantMapper.toVariantSearchResultDTO(variant));
			vsrInMemoryRepository.countAndFillPercentage(variantSearchResultDTOPage, searchedSampleIds);

		} else if (snp142SearchMatcher.matches()) {
			theVariant = variantRepository.findOneBySnp142(variantSearchPhrase);

			if(theVariant == null)
				return new PageImpl<VariantSearchResultDTO>(new ArrayList<VariantSearchResultDTO>(), pageable, 0); 

			variantSearchResultDTOPage = new PageImpl<>(Arrays.asList(variantMapper.toVariantSearchResultDTO(theVariant)), pageable, 1);

		} else 
			 throw new InvalidSearchPhraseException("Unsupported search phrase: " + variantSearchPhrase);
		return variantSearchResultDTOPage;
	}
	
	@Override
	public VariantDTO findOne(String id) {
		log.debug("Request to get Variant : {}", id);
		Variant variant = variantRepository.findOne(id);
		VariantDTO result = variantMapper.toDto(variant);
		ChromosomePosition chP = VariantUtil.getChromosome(variant.getPosition());
		result.setChr(chP.getChr());
		result.setPosition(chP.getRelativePosition().longValue());
		return result;
	}
	
	@Override
	public VariantSampleDiseasePrevalenceDTO findOnePrevalenceDTO(String variantId) throws CorruptVariantSampleRelationException {
		log.debug("Request to get Variant extra: {}", variantId);
		return vsrInMemoryRepository.getVariantExtra(variantId);
	}

}