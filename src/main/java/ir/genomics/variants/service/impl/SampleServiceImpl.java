package ir.genomics.variants.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.io.Closeables;
import com.hazelcast.core.IMap;

import ir.genomics.variants.config.ApplicationProperties;
import ir.genomics.variants.domain.Disease;
import ir.genomics.variants.domain.Race;
import ir.genomics.variants.domain.Sample;
import ir.genomics.variants.domain.enumeration.Gender;
import ir.genomics.variants.domain.enumeration.Province;
import ir.genomics.variants.domain.enumeration.SampleImportingStatus;
import ir.genomics.variants.domain.enumeration.Zygosity;
import ir.genomics.variants.repository.DiseaseRepository;
import ir.genomics.variants.repository.RaceRepository;
import ir.genomics.variants.repository.VariantSampleRelationInDiskRepository;
import ir.genomics.variants.repository.VariantSampleRelationInMemoryRepository;
import ir.genomics.variants.repository.sample.SampleRepository;
import ir.genomics.variants.repository.variant.VariantInMemoryRepository;
import ir.genomics.variants.service.SampleService;
import ir.genomics.variants.service.dto.SampleDTO;
import ir.genomics.variants.service.dto.SampleWithVariantZygosityDTO;
import ir.genomics.variants.service.mapper.SampleMapper;
import ir.genomics.variants.service.util.SampleAsyncOperations;

/**
 * Service Implementation for managing Sample.
 */
@Service
@Transactional
public class SampleServiceImpl implements SampleService {

	private final Logger log = LoggerFactory.getLogger(SampleServiceImpl.class);

	private final SampleRepository sampleRepository;

	private final SampleMapper sampleMapper;

	private final ApplicationProperties applicationProperties;

	private final SampleAsyncOperations sampleAsyncOperations;

	private final RaceRepository raceRepository;
	
	private final DiseaseRepository diseaseRepository;
	
	private final VariantSampleRelationInMemoryRepository vsrInMemoryRepository;

	public SampleServiceImpl(SampleRepository sampleRepository, SampleMapper sampleMapper,
			ApplicationProperties applicationProperties, MongoTemplate mongoTemplate,
			VariantSampleRelationInMemoryRepository vsrInMemoryRepository, SampleAsyncOperations sampleAsyncOperations,
			RaceRepository raceRepository, DiseaseRepository diseaseRepository) {
		this.sampleRepository = sampleRepository;
		this.sampleMapper = sampleMapper;
		this.applicationProperties = applicationProperties;
		this.sampleAsyncOperations = sampleAsyncOperations;
		this.raceRepository = raceRepository;
		this.diseaseRepository = diseaseRepository;
		this.vsrInMemoryRepository = vsrInMemoryRepository;
	}

	@Async("taskExecutor")
	public void cleanUpSampleDataAsync() {

	}

	/**
     * Upload a sample.
     *
     * @param request the HttpServletRequest which contains octet-stream upload request
     * @return the persisted entity
     * @throws IOException 
     * @throws FileUploadException 
     */
	@Override
	public SampleDTO uploadSample(HttpServletRequest request) throws IOException  {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (!isMultipart) {         
        	// Inform user about invalid request         }
        }
        
        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload();
		// TODO Auto-generated method stub
        
        String firstName = null;
        String lastName = null;
        Integer age = null;
        String nationalCode = null;
        LocalDate birthDate = null;
        LocalDate deathDate = null;
        Gender gender = null;
        Province birthProvince = null;
        Province livingProvince = null;
        List<Long> raceIds = null;
        List<Race> races = null;
        List<Long> diseaseIds = null;
        List<Disease> diseases = null;
        
        
        
        InputStream stream = null;
        Sample newSample = null;
        boolean fileProcessed = false; 
        
        // Parse the request
        try {
	        FileItemIterator iter = upload.getItemIterator(request);
			while (iter.hasNext()) {
				FileItemStream item = iter.next();
				String name = item.getFieldName();
				stream = item.openStream();
				if (item.isFormField()) {
					
//					System.out.println("Form field " + name + " with value " +  + " detected.");
					switch(name) {
					case "firstName":
						firstName = Streams.asString(stream);
						break;
					case "lastName":
						lastName = Streams.asString(stream);
						break;
					case "age":
						age = Integer.parseInt(Streams.asString(stream));
						break;
					case "nationalCode":
						nationalCode = Streams.asString(stream);
						break;
					case "birthDate":
						//TODO
						String str = Streams.asString(stream);
						break;
					case "deathDate":
						//TODO
						str = Streams.asString(stream);
						break;
					case "gender":
						gender = Gender.valueOf(Streams.asString(stream));
//						gender = Gender.parse(Integer.parseInt(Streams.asString(stream)));
						break;
					case "birthProvince":
						birthProvince = Province.valueOf(Streams.asString(stream));
						break;
					case "livingProvince":
						livingProvince = Province.valueOf(Streams.asString(stream));
						break;
					case "races":
						str = Streams.asString(stream);
						if(StringUtils.isEmpty(str) || StringUtils.isEmpty(str.trim()))
							break;
						raceIds = Arrays.asList(str.trim().split(","))
										.stream()
										.map(String::trim)
										.mapToLong(Long::parseLong).boxed().collect(Collectors.toList());
						races = raceRepository.findAll(raceIds);
						break;
					case "diseases":
						str = Streams.asString(stream);
						if(StringUtils.isEmpty(str) || StringUtils.isEmpty(str.trim()))
							break;
						diseaseIds = Arrays.asList(str.trim().split(","))
										.stream()
										.map(String::trim)
										.mapToLong(Long::parseLong).boxed().collect(Collectors.toList());
						diseases = diseaseRepository.findAll(diseaseIds);
						break;
					}
				} else {
					System.out.println("File field " + name + " with file name " + item.getName() + " detected.");
					Path pathToFolder = Paths.get(System.getProperty("user.home") + File.separator + applicationProperties.getUploadDirectory());
	                Files.createDirectories(pathToFolder);
	                String fileHash = UUID.randomUUID().toString();
	                File uploadFile = new File(pathToFolder.toAbsolutePath()+ File.separator + fileHash);
	                
	                FileUtils.copyInputStreamToFile(stream, uploadFile);
	                
	                newSample = new Sample();
	                newSample.setFileOriginalName(item.getName());
	                newSample.setFileHashName(fileHash);
	                
	                fileProcessed = true;
	                
	                break;
				}
			}
        } catch (FileUploadException e) {
        	throw new IOException();
        } finally {
        	Closeables.closeQuietly(stream);
        }
        
        if(fileProcessed) {
        	newSample.setImportingStatus(SampleImportingStatus.NOT_FINISHED);
        	newSample.setFirstName(firstName);
        	newSample.setLastName(lastName);
        	newSample.setAge(age);
        	newSample.setNationalCode(nationalCode);
//        	newSample.setBirthDate(birthDate);
//        	newSample.setDeathDate(deathDate);
        	newSample.setGender(gender);
        	newSample.setBirthProvince(birthProvince);
        	newSample.setLivingProvince(livingProvince);
        	if(!CollectionUtils.isEmpty(races))
        		newSample.setRaces(new HashSet<Race>(races));
        	if(!CollectionUtils.isEmpty(diseases))
        		newSample.setDiseases(new HashSet<Disease>(diseases));
            sampleRepository.save(newSample);
            
            // this line is non blocking:
            SampleDTO dto = sampleMapper.toDto(newSample);
            sampleAsyncOperations.importSampleDataAsync(dto);
            System.out.println("SERVICE CONTINUED...");
            
        	return dto;
        }
        else 
            throw new IllegalStateException("Unable to find supplied data file!");
        
	}

	/**
	 * Save a sample.
	 *
	 * @param sampleDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public SampleDTO save(SampleDTO sampleDTO) {
		log.debug("Request to save Sample : {}", sampleDTO);
		Sample sample = sampleMapper.toEntity(sampleDTO);
		sample = sampleRepository.save(sample);
		return sampleMapper.toDto(sample);
	}

	/**
	 * Get all the samples.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<SampleDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Samples");
		return sampleRepository.findAll(pageable).map(sampleMapper::toDto);
	}

	/**
	 * Get one sample by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public SampleDTO findOne(Long id) {
		log.debug("Request to get Sample : {}", id);
		Sample sample = sampleRepository.findOne(id);
		return sampleMapper.toDto(sample);
	}

	/**
	 * Delete the sample by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Sample : {}", id);
		sampleRepository.delete(id);
	}

	@Override
	public List<SampleWithVariantZygosityDTO> findAllEngaging(String variantId, Long diseaseId) {
		List<SampleWithVariantZygosityDTO> result = new ArrayList<SampleWithVariantZygosityDTO>();
		List<Sample> samples = sampleRepository.findByDiseasesId(diseaseId);
		if(CollectionUtils.isEmpty(samples))
			return null;
		List<String> keysToSearch = new ArrayList<String>(samples.size());
		for(Sample s: samples)
			keysToSearch.add(s.getId() + variantId);
		
		IMap<String, Zygosity> map = vsrInMemoryRepository.getMap();
		for(int i=0; i<keysToSearch.size(); i++) {
			String key = keysToSearch.get(i);
			if(map.containsKey(key)) {
				SampleWithVariantZygosityDTO newItem = sampleMapper.toSampleWithVariantZygosityDTO(samples.get(i));
				newItem.setVariantZygosity(map.get(key));
				result.add(newItem);
			}
		}
		
		return result;
	}

}
