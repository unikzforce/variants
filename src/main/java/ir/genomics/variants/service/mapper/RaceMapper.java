package ir.genomics.variants.service.mapper;

import ir.genomics.variants.domain.*;
import ir.genomics.variants.service.dto.RaceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Race and its DTO RaceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RaceMapper extends EntityMapper<RaceDTO, Race> {



    default Race fromId(Long id) {
        if (id == null) {
            return null;
        }
        Race race = new Race();
        race.setId(id);
        return race;
    }
}
