package ir.genomics.variants.service.mapper;

import ir.genomics.variants.domain.*;
import ir.genomics.variants.service.dto.SampleDTO;
import ir.genomics.variants.service.dto.SampleWithVariantZygosityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Sample and its DTO SampleDTO.
 */
@Mapper(componentModel = "spring", uses = {RaceMapper.class, DiseaseMapper.class})
public interface SampleMapper extends EntityMapper<SampleDTO, Sample> {

	SampleWithVariantZygosityDTO toSampleWithVariantZygosityDTO(Sample samlpe);

    default Sample fromId(Long id) {
        if (id == null) {
            return null;
        }
        Sample sample = new Sample();
        sample.setId(id);
        return sample;
    }
}
