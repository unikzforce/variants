package ir.genomics.variants.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import ir.genomics.variants.domain.Sample;
import ir.genomics.variants.domain.Variant;
import ir.genomics.variants.service.dto.SampleDTO;
import ir.genomics.variants.service.dto.VariantDTO;
import ir.genomics.variants.service.dto.VariantSearchResultDTO;
import ir.genomics.variants.service.util.ChromosomePosition;
import ir.genomics.variants.service.util.VariantUtil;

@Mapper(componentModel = "spring", uses = {})
public abstract class VariantMapper implements EntityMapper<VariantDTO, Variant> {

	public VariantSearchResultDTO toVariantSearchResultDTO(Variant variant) {
		VariantSearchResultDTO result = new VariantSearchResultDTO();
		result.setId(variant.getId());
		ChromosomePosition chp = VariantUtil.getChromosome(variant.getPosition());
		result.setChr(chp.getChr());
		result.setPosition(chp.getRelativePosition());
		result.setRef(variant.getRef());
		result.setAlt(variant.getAlt());
		result.setRefGeneAAChange(variant.getRefGeneAAChange());
		result.setRefGene(variant.getRefGene());
		result.setRefGeneExonicFunc(variant.getRefGeneExonicFunc());
		result.setRefGeneFunc(variant.getRefGeneFunc());
		return result;
	}

	public abstract List<VariantSearchResultDTO> toVariantSearchResultDTO(List<Variant> variants);
}
