package ir.genomics.variants.service.dto;

public class VariantDTO {
	
	private String id;
	
	private String chr;
	
	private Long position;
	
	private String ref;
	
	private String alt;
	
	private String ensGene;
	
	private String refGene;
	
	private String refGeneFunc;
	
	private String refGeneExonicFunc;
	
	private String refGeneAAChange;
	
	private String esp6500SIV2All;
	
	private String the1000g2015AugAll;
	
	private String exacAll;
	
	private String cg69;
	
	private String snp142;
	
	private String clinsig;
	
	private String clndbn;
	
	private String clnacc;
	
	private String clndsdbid;
	
	private String siftScore;
	
	private String polyphen2HDIVScore;
	
	private String caddRaw;
	
	private String caddPhred;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChr() {
		return chr;
	}

	public void setChr(String chr) {
		this.chr = chr;
	}

	public Long getPosition() {
		return position;
	}

	public void setPosition(Long position) {
		this.position = position;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public String getEnsGene() {
		return ensGene;
	}

	public void setEnsGene(String ensGene) {
		this.ensGene = ensGene;
	}

	public String getRefGene() {
		return refGene;
	}

	public void setRefGene(String refGene) {
		this.refGene = refGene;
	}

	public String getRefGeneFunc() {
		return refGeneFunc;
	}

	public void setRefGeneFunc(String refGeneFunc) {
		this.refGeneFunc = refGeneFunc;
	}

	public String getRefGeneExonicFunc() {
		return refGeneExonicFunc;
	}

	public void setRefGeneExonicFunc(String refGeneExonicFunc) {
		this.refGeneExonicFunc = refGeneExonicFunc;
	}

	public String getRefGeneAAChange() {
		return refGeneAAChange;
	}

	public void setRefGeneAAChange(String refGeneAAChange) {
		this.refGeneAAChange = refGeneAAChange;
	}

	public String getEsp6500SIV2All() {
		return esp6500SIV2All;
	}

	public void setEsp6500SIV2All(String esp6500siv2All) {
		esp6500SIV2All = esp6500siv2All;
	}

	public String getThe1000g2015AugAll() {
		return the1000g2015AugAll;
	}

	public void setThe1000g2015AugAll(String the1000g2015AugAll) {
		this.the1000g2015AugAll = the1000g2015AugAll;
	}

	public String getExacAll() {
		return exacAll;
	}

	public void setExacAll(String exacAll) {
		this.exacAll = exacAll;
	}

	public String getCg69() {
		return cg69;
	}

	public void setCg69(String cg69) {
		this.cg69 = cg69;
	}

	public String getSnp142() {
		return snp142;
	}

	public void setSnp142(String snp142) {
		this.snp142 = snp142;
	}

	public String getClinsig() {
		return clinsig;
	}

	public void setClinsig(String clinsig) {
		this.clinsig = clinsig;
	}

	public String getClndbn() {
		return clndbn;
	}

	public void setClndbn(String clndbn) {
		this.clndbn = clndbn;
	}

	public String getClnacc() {
		return clnacc;
	}

	public void setClnacc(String clnacc) {
		this.clnacc = clnacc;
	}

	public String getClndsdbid() {
		return clndsdbid;
	}

	public void setClndsdbid(String clndsdbid) {
		this.clndsdbid = clndsdbid;
	}

	public String getSiftScore() {
		return siftScore;
	}

	public void setSiftScore(String siftScore) {
		this.siftScore = siftScore;
	}

	public String getPolyphen2HDIVScore() {
		return polyphen2HDIVScore;
	}

	public void setPolyphen2HDIVScore(String polyphen2hdivScore) {
		polyphen2HDIVScore = polyphen2hdivScore;
	}

	public String getCaddRaw() {
		return caddRaw;
	}

	public void setCaddRaw(String caddRaw) {
		this.caddRaw = caddRaw;
	}

	public String getCaddPhred() {
		return caddPhred;
	}

	public void setCaddPhred(String caddPhred) {
		this.caddPhred = caddPhred;
	}
}
