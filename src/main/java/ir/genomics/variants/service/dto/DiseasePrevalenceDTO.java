package ir.genomics.variants.service.dto;

public class DiseasePrevalenceDTO {
	private Long id;
	
	private String name;
	
	private int prevalence;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrevalence() {
		return prevalence;
	}

	public void setPrevalence(int prevalence) {
		this.prevalence = prevalence;
	}
}
