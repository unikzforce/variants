package ir.genomics.variants.service.dto;

import java.util.List;

public class VariantSampleDiseasePrevalenceDTO {
	
	private String id;
	
	private int sampleCount;
	
	private List<DiseasePrevalenceDTO> prevalences;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getSampleCount() {
		return sampleCount;
	}

	public void setSampleCount(int sampleCount) {
		this.sampleCount = sampleCount;
	}

	public List<DiseasePrevalenceDTO> getPrevalences() {
		return prevalences;
	}

	public void setPrevalences(List<DiseasePrevalenceDTO> prevalences) {
		this.prevalences = prevalences;
	}
}
