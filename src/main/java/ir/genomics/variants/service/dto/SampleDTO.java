package ir.genomics.variants.service.dto;


import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import ir.genomics.variants.domain.enumeration.Gender;
import ir.genomics.variants.domain.enumeration.Province;
import ir.genomics.variants.domain.enumeration.SampleImportingStatus;

/**
 * A DTO for the Sample entity.
 */
public class SampleDTO implements Serializable {

    private Long id;

    private String fileOriginalName;

    private String fileHashName;

    private String firstName;

    private String lastName;

    private Integer age;

    private String nationalCode;

    private LocalDate birthDate;

    private LocalDate deathDate;

    private Gender gender;

    private Province birthProvince;

    private Province livingProvince;

    private SampleImportingStatus importingStatus;

    private Set<RaceDTO> races = new HashSet<>();

    private Set<DiseaseDTO> diseases = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileOriginalName() {
        return fileOriginalName;
    }

    public void setFileOriginalName(String fileOriginalName) {
        this.fileOriginalName = fileOriginalName;
    }

    public String getFileHashName() {
        return fileHashName;
    }

    public void setFileHashName(String fileHashName) {
        this.fileHashName = fileHashName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDate getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(LocalDate deathDate) {
        this.deathDate = deathDate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Province getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(Province birthProvince) {
        this.birthProvince = birthProvince;
    }

    public Province getLivingProvince() {
        return livingProvince;
    }

    public void setLivingProvince(Province livingProvince) {
        this.livingProvince = livingProvince;
    }

    public SampleImportingStatus getImportingStatus() {
        return importingStatus;
    }

    public void setImportingStatus(SampleImportingStatus importingStatus) {
        this.importingStatus = importingStatus;
    }

    public Set<RaceDTO> getRaces() {
        return races;
    }

    public void setRaces(Set<RaceDTO> races) {
        this.races = races;
    }

    public Set<DiseaseDTO> getDiseases() {
        return diseases;
    }

    public void setDiseases(Set<DiseaseDTO> diseases) {
        this.diseases = diseases;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SampleDTO sampleDTO = (SampleDTO) o;
        if(sampleDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sampleDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SampleDTO{" +
            "id=" + getId() +
            ", fileOriginalName='" + getFileOriginalName() + "'" +
            ", fileHashName='" + getFileHashName() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", age=" + getAge() +
            ", nationalCode='" + getNationalCode() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", deathDate='" + getDeathDate() + "'" +
            ", gender='" + getGender() + "'" +
            ", birthProvince='" + getBirthProvince() + "'" +
            ", livingProvince='" + getLivingProvince() + "'" +
            ", importingStatus='" + getImportingStatus() + "'" +
            "}";
    }
}
