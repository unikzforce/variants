package ir.genomics.variants.service.dto;

public class VariantSearchResultDTO {
	
	private String id;
	
	private String chr;
	
	private Integer position;
	
	private String ref;
	
	private String alt;
	
	private String refGeneAAChange;
	
	private String refGeneFunc;
	
	private String refGene;
	
	private String refGeneExonicFunc;
	
	private double alleleFrequency;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getChr() {
		return chr;
	}

	public void setChr(String chr) {
		this.chr = chr;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public String getRefGeneAAChange() {
		return refGeneAAChange;
	}

	public void setRefGeneAAChange(String refGeneAAChange) {
		this.refGeneAAChange = refGeneAAChange;
	}

	public String getRefGeneFunc() {
		return refGeneFunc;
	}

	public void setRefGeneFunc(String refGeneFunc) {
		this.refGeneFunc = refGeneFunc;
	}

	public String getRefGene() {
		return refGene;
	}

	public void setRefGene(String refGene) {
		this.refGene = refGene;
	}

	public String getRefGeneExonicFunc() {
		return refGeneExonicFunc;
	}

	public void setRefGeneExonicFunc(String refGeneExonicFunc) {
		this.refGeneExonicFunc = refGeneExonicFunc;
	}

	public double getAlleleFrequency() {
		return alleleFrequency;
	}

	public void setAlleleFrequency(double alleleFrequency) {
		this.alleleFrequency = alleleFrequency;
	}

}
