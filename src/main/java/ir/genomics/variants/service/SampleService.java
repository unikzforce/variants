package ir.genomics.variants.service;

import ir.genomics.variants.service.dto.SampleDTO;
import ir.genomics.variants.service.dto.SampleWithVariantZygosityDTO;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Sample.
 */
public interface SampleService {
	
	
	SampleDTO uploadSample(HttpServletRequest request) throws FileUploadException, IOException;

    /**
     * Save a sample.
     *
     * @param sampleDTO the entity to save
     * @return the persisted entity
     */
    SampleDTO save(SampleDTO sampleDTO);

    /**
     * Get all the samples.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SampleDTO> findAll(Pageable pageable);

    /**
     * Get the "id" sample.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SampleDTO findOne(Long id);

    /**
     * Delete the "id" sample.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

	List<SampleWithVariantZygosityDTO> findAllEngaging(String variantId, Long diseaseId);
}
