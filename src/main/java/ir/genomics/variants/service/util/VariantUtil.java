package ir.genomics.variants.service.util;

import ir.genomics.variants.domain.enumeration.Chromosome;
import ir.genomics.variants.service.util.exception.ChromosomeRelativeToAbsolutePositionConversionException;

public class VariantUtil {
	
	public static long getPosition(ChromosomePosition chp) throws ChromosomeRelativeToAbsolutePositionConversionException {
		return VariantUtil.getAbsolutePosition(Chromosome.parse(chp.getChr()), chp.getRelativePosition());
	}
	
	public static ChromosomePosition getChromosome(long absolutePosition) {
		ChromosomePosition result = new ChromosomePosition();
		if(  0 < absolutePosition && absolutePosition <= 249250621 ) {
			result.setChr("1");
			result.setRelativePosition((int)absolutePosition);
		} else if (  249250621 < absolutePosition  &&  absolutePosition <= 492449994 ) {
			result.setChr("2");
			result.setRelativePosition((int)(absolutePosition- 249250621));
		} else if (  492449994 < absolutePosition  &&  absolutePosition <= 690472424 ) {
			result.setChr("3");
			result.setRelativePosition((int)(absolutePosition- 492449994));
		} else if (  690472424 < absolutePosition  &&  absolutePosition <= 881626700 ) {
			result.setChr("4");
			result.setRelativePosition((int)(absolutePosition- 690472424));
		} else if (  881626700 < absolutePosition  &&  absolutePosition <= 1062541960 ) {
			result.setChr("5");
			result.setRelativePosition((int)(absolutePosition- 881626700));
		} else if (  1062541960 < absolutePosition  &&  absolutePosition <= 1233657027 ) {
			result.setChr("6");
			result.setRelativePosition((int)(absolutePosition- 1062541960));
		} else if (  1233657027 < absolutePosition  &&  absolutePosition <= 1392795690 ) {
			result.setChr("7");
			result.setRelativePosition((int)(absolutePosition- 1233657027));
		} else if (  1392795690 < absolutePosition  &&  absolutePosition <= 1539159712 ) {
			result.setChr("8");
			result.setRelativePosition((int)(absolutePosition- 1392795690));
		} else if (  1539159712 < absolutePosition  &&  absolutePosition <= 1680373143 ) {
			result.setChr("9");
			result.setRelativePosition((int)(absolutePosition- 1539159712));
		} else if (  1680373143 < absolutePosition  &&  absolutePosition <= 1815907890 ) {
			result.setChr("10");
			result.setRelativePosition((int)(absolutePosition- 1680373143));
		} else if (  1815907890 < absolutePosition  &&  absolutePosition <= 1950914406 ) {
			result.setChr("11");
			result.setRelativePosition((int)(absolutePosition- 1815907890));
		} else if (  1950914406 < absolutePosition  &&  absolutePosition <= 2084766301 ) {
			result.setChr("12");
			result.setRelativePosition((int)(absolutePosition- 1950914406));
		} else if (  2084766301 < absolutePosition  &&  absolutePosition <= 2199936179L ) {
			result.setChr("13");
			result.setRelativePosition((int)(absolutePosition- 2084766301));
		} else if (  2199936179L < absolutePosition  &&  absolutePosition <= 2307285719L ) {
			result.setChr("14");
			result.setRelativePosition((int)(absolutePosition- 2199936179L));
		} else if (  2307285719L < absolutePosition  &&  absolutePosition <= 2409817111L ) {
			result.setChr("15");
			result.setRelativePosition((int)(absolutePosition- 2307285719L));
		} else if (  2409817111L < absolutePosition  &&  absolutePosition <= 2500171864L ) {
			result.setChr("16");
			result.setRelativePosition((int)(absolutePosition- 2409817111L));
		} else if (  2500171864L < absolutePosition  &&  absolutePosition <= 2581367074L ) {
			result.setChr("17");
			result.setRelativePosition((int)(absolutePosition- 2500171864L));
		} else if (  2581367074L < absolutePosition  &&  absolutePosition <= 2659444322L ) {
			result.setChr("18");
			result.setRelativePosition((int)(absolutePosition- 2581367074L));
		} else if (  2659444322L < absolutePosition  &&  absolutePosition <= 2718573305L ) {
			result.setChr("19");
			result.setRelativePosition((int)(absolutePosition- 2659444322L));
		} else if (  2718573305L < absolutePosition  &&  absolutePosition <= 2781598825L ) {
			result.setChr("20");
			result.setRelativePosition((int)(absolutePosition- 2718573305L));
		} else if (  2781598825L < absolutePosition  &&  absolutePosition <= 2829728720L ) {
			result.setChr("21");
			result.setRelativePosition((int)(absolutePosition- 2781598825L));
		} else if (  2829728720L < absolutePosition  &&  absolutePosition <= 2881033286L ) {
			result.setChr("22");
			result.setRelativePosition((int)(absolutePosition- 2829728720L));
		} else if (  2881033286L < absolutePosition  &&  absolutePosition <= 3036303846L ) {
			result.setChr("x");
			result.setRelativePosition((int)(absolutePosition- 2881033286L));
		} else if (  3036303846L < absolutePosition  &&  absolutePosition <= 3095677412L ) {
			result.setChr("y");
			result.setRelativePosition((int)(absolutePosition- 3036303846L));
		} else if (  3095677412L < absolutePosition  &&  absolutePosition <= 3095693983L ) {
			result.setChr("m");
			result.setRelativePosition((int)(absolutePosition- 3095677412L));
		}
		return result;
	}
	
	public static long getAbsolutePosition(Chromosome chr, int relativePosition) throws ChromosomeRelativeToAbsolutePositionConversionException {
		if(relativePosition <= 0)
			throw new ChromosomeRelativeToAbsolutePositionConversionException();
		long sum = 0;
		switch(chr) {
		case CHR_1:
			if(relativePosition > 249250621)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return relativePosition;
		case CHR_2:
			if(relativePosition > 243199373)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return sum;
		case CHR_3:
			if(relativePosition > 198022430)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 492449994 + relativePosition; 
		case CHR_4:
			if(relativePosition > 191154276)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 690472424 + relativePosition; 
		case CHR_5:
			if(relativePosition > 180915260)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 881626700 + relativePosition; 
		case CHR_6:
			if(relativePosition > 171115067)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 1062541960 + relativePosition; 
		case CHR_7:
			if(relativePosition > 159138663)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 1233657027 + relativePosition; 
		case CHR_8:
			if(relativePosition > 146364022)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 1392795690 + relativePosition; 
		case CHR_9:
			if(relativePosition > 141213431)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 1539159712 + relativePosition; 
		case CHR_10:
			if(relativePosition > 135534747)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 1680373143 + relativePosition; 
		case CHR_11:
			if(relativePosition > 135006516)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 1815907890 + relativePosition; 
		case CHR_12:
			if(relativePosition > 133851895)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 1950914406 + relativePosition; 
		case CHR_13:
			if(relativePosition > 115169878)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2084766301 + relativePosition; 
		case CHR_14:
			if(relativePosition > 107349540)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2199936179L + relativePosition; 
		case CHR_15:
			if(relativePosition > 102531392)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2307285719L + relativePosition; 
		case CHR_16:
			if(relativePosition > 90354753)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2409817111L + relativePosition; 
		case CHR_17:
			if(relativePosition > 81195210)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2500171864L + relativePosition; 
		case CHR_18:
			if(relativePosition > 78077248)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2581367074L + relativePosition; 
		case CHR_19:
			if(relativePosition > 59128983)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2659444322L + relativePosition; 
		case CHR_20:
			if(relativePosition > 63025520)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2718573305L + relativePosition; 
		case CHR_21:
			if(relativePosition > 48129895)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2781598825L + relativePosition; 
		case CHR_22:
			if(relativePosition > 51304566)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2829728720L + relativePosition; 
		case CHR_X:
			if(relativePosition > 155270560)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 2881033286L + relativePosition; 
		case CHR_Y:
			if(relativePosition > 59373566)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 3036303846L + relativePosition; 
		case CHR_M:
			if(relativePosition > 16571)
				throw new ChromosomeRelativeToAbsolutePositionConversionException();
			return 3095677412L + relativePosition; 
		default:
			throw new ChromosomeRelativeToAbsolutePositionConversionException();
		}
	}
	
}
