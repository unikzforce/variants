package ir.genomics.variants.service.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.mongodb.BulkWriteResult;
import com.mongodb.BulkWriteUpsert;
import com.univocity.parsers.tsv.TsvParser;
import com.univocity.parsers.tsv.TsvParserSettings;

import ir.genomics.variants.config.ApplicationProperties;
import ir.genomics.variants.domain.QVariant;
import ir.genomics.variants.domain.Sample;
import ir.genomics.variants.domain.Variant;
import ir.genomics.variants.domain.VariantMini;
import ir.genomics.variants.domain.enumeration.Chromosome;
import ir.genomics.variants.domain.enumeration.SampleImportingStatus;
import ir.genomics.variants.domain.enumeration.Zygosity;
import ir.genomics.variants.repository.VariantSampleRelationInDiskRepository;
import ir.genomics.variants.repository.VariantSampleRelationInMemoryRepository;
import ir.genomics.variants.repository.sample.SampleRepository;
import ir.genomics.variants.service.SampleService;
import ir.genomics.variants.service.dto.SampleDTO;
import ir.genomics.variants.service.impl.SampleServiceImpl;
import ir.genomics.variants.service.util.exception.ChromosomeRelativeToAbsolutePositionConversionException;

@Component
public class SampleAsyncOperations {

	private final String FIELD_NAME_POSITION = QVariant.variant.position.getMetadata().getName();
	private final String FIELD_NAME_REF = QVariant.variant.ref.getMetadata().getName();
	private final String FIELD_NAME_ALT = QVariant.variant.alt.getMetadata().getName();
	private final String FIELD_NAME_ENS_GENE = QVariant.variant.ensGene.getMetadata().getName();
	private final String FIELD_NAME_REF_GENE = QVariant.variant.refGene.getMetadata().getName();
	private final String FIELD_NAME_REF_GENE_FUNC = QVariant.variant.refGeneFunc.getMetadata().getName();
	private final String FIELD_NAME_REF_GENE_EXONIC_FUNC = QVariant.variant.refGeneExonicFunc.getMetadata().getName();
	private final String FIELD_NAME_REF_GENE_AA_CHANGE = QVariant.variant.refGeneAAChange.getMetadata().getName();
	private final String FIELD_NAME_ESP6500SIV2ALL = QVariant.variant.esp6500SIV2All.getMetadata().getName();
	private final String FIELD_NAME_THOUSANDG2015AUG_ALL = QVariant.variant.the1000g2015AugAll.getMetadata().getName();
	private final String FIELD_NAME_EXAC_ALL = QVariant.variant.exacAll.getMetadata().getName();
	private final String FIELD_NAME_CG69 = QVariant.variant.cg69.getMetadata().getName();
	private final String FIELD_NAME_SNP142 = QVariant.variant.snp142.getMetadata().getName();
	private final String FIELD_NAME_CLINSIG = QVariant.variant.clinsig.getMetadata().getName();
	private final String FIELD_NAME_CLNDBN = QVariant.variant.clndbn.getMetadata().getName();
	private final String FIELD_NAME_CLNACC = QVariant.variant.clnacc.getMetadata().getName();
	private final String FIELD_NAME_CLNDSDBID = QVariant.variant.clndsdbid.getMetadata().getName();
	private final String FIELD_NAME_SIFT_SCORE = QVariant.variant.siftScore.getMetadata().getName();
	private final String FIELD_NAME_POLYPHEN2_HDIV_SCORE = QVariant.variant.polyphen2HDIVScore.getMetadata().getName();
	private final String FIELD_NAME_CADD_RAW = QVariant.variant.caddRaw.getMetadata().getName();
	private final String FIELD_NAME_CADD_PHRED = QVariant.variant.caddPhred.getMetadata().getName();

	private final int IMPORT_SIZE = 2000;

	private class PositionAltPair {
		long position;
		String alt;

		public PositionAltPair(long position, String alt) {
			this.position = position;
			this.alt = alt;
		}

		@Override
		public boolean equals(Object obj) {

			PositionAltPair other = (PositionAltPair) obj;

			if (other.position == this.position && other.alt.equals(other.alt))
				return true;
			return false;
		}
	}

	private final SampleRepository sampleRepository;

	private final MongoTemplate mongoTemplate;

	private final ApplicationProperties applicationProperties;

	private final VariantSampleRelationInDiskRepository vsrInDiskRepository;

	private final VariantSampleRelationInMemoryRepository vsrInMemoryRepository;

	public SampleAsyncOperations(SampleRepository sampleRepository, MongoTemplate mongoTemplate,
			ApplicationProperties applicationProperties, VariantSampleRelationInDiskRepository vsrInDiskRepository,
			VariantSampleRelationInMemoryRepository vsrInMemoryRepository) {
		this.sampleRepository = sampleRepository;
		this.applicationProperties = applicationProperties;
		this.mongoTemplate = mongoTemplate;
		this.vsrInDiskRepository = vsrInDiskRepository;
		this.vsrInMemoryRepository = vsrInMemoryRepository;
	}

	@Async("taskExecutor")
	public void importSampleDataAsync(SampleDTO sampleDTO) throws IOException {

		// TODO HANDLE ERRORS TODO HANDLE ERRORS
		System.out.println("AsyncStarted");
		TsvParserSettings settings = new TsvParserSettings();
		settings.setMaxCharsPerColumn(-1);
		settings.getFormat().setLineSeparator("\n");
		TsvParser parser = new TsvParser(settings);

		final String dot = ".";

		Table vsrTable = null;

		try {
			Path pathToFolder = Paths
					.get(System.getProperty("user.home") + File.separator + applicationProperties.getUploadDirectory());
			File uploadedFile = new File(pathToFolder.toAbsolutePath() + File.separator + sampleDTO.getFileHashName());

			// import from file to variants collection in mongodb
			BulkOperations ops = mongoTemplate.bulkOps(BulkMode.ORDERED, Variant.class);
			// each insert should insert 3000 variants, more than that could lead to heap
			// starvation(?)(benchmark it)

			// WHILE LOOP variables
			int iterationCounter = 0;

			List<Zygosity> zygoList = new ArrayList<Zygosity>(IMPORT_SIZE);
			List<PositionAltPair> pairList = new ArrayList<PositionAltPair>(IMPORT_SIZE);
			// List<VariantMini> miniVariants = new ArrayList<>(IMPORT_SIZE);

			String[] row;
			// VariantMini newVariantMini = null;
			Update newUpdate = null;
			Query newQuery = null;
			parser.beginParsing(uploadedFile);

			// read the first row to ignore column titles
			row = parser.parseNext();

			for (int i = 0; i < row.length; i++)
				row[i] = row[i].toLowerCase();

			int CHR_INDEX = ArrayUtils.indexOf(row, "chr");
			int START_INDEX = ArrayUtils.indexOf(row, "start");
			// int END_INDEX = ArrayUtils.indexOf(row, "end");
			int REF_INDEX = ArrayUtils.indexOf(row, "ref");
			int ALT_INDEX = ArrayUtils.indexOf(row, "alt");
			int ZYGOSITY_INDEX = ArrayUtils.indexOf(row, "zygous");
			int ENS_GENE_GENE_INDEX = ArrayUtils.indexOf(row, "gene.ensgene");
			int REF_GENE_FUNC_INDEX = ArrayUtils.indexOf(row, "func.refgene");
			int REF_GENE_INDEX = ArrayUtils.indexOf(row, "gene.refgene");
			int REF_GENE_EXONIC_FUNC = ArrayUtils.indexOf(row, "exonicfunc.refgene");
			int REF_GENE_AA_CHANGE_INDEX = ArrayUtils.indexOf(row, "aachange.refgene");
			int ESP6500SIV2_ALL_INDEX = ArrayUtils.indexOf(row, "esp6500siv2_all");
			int THOUSANDG2015AUG_ALL_INDEX = ArrayUtils.indexOf(row, "1000g2015aug_all");
			int EXAC_ALL_INDEX = ArrayUtils.indexOf(row, "exac_all");
			int CG69_INDEX = ArrayUtils.indexOf(row, "cg69");
			int SNP142_INDEX = ArrayUtils.indexOf(row, "snp142");
			int CLINSIG_INDEX = ArrayUtils.indexOf(row, "clinsig");
			int CLNDBN_INDEX = ArrayUtils.indexOf(row, "clndbn");
			int CLNACC_INDEX = ArrayUtils.indexOf(row, "clnacc");
			int CLNDSDBID_INDEX = ArrayUtils.indexOf(row, "clndsdbid");
			int SIFT_SCORE_INDEX = ArrayUtils.indexOf(row, "sift_score");
			int POLYPHEN2_HDIV_SCORE_INDEX = ArrayUtils.indexOf(row, "polyphen2_hdiv_score");
			int CADD_RAW_INDEX = ArrayUtils.indexOf(row, "cadd_raw");
			int CADD_PHRED_INDEX = ArrayUtils.indexOf(row, "cadd_phred");

			vsrTable = vsrInDiskRepository.openVsrTable();

			while (true) {

				row = parser.parseNext();

				if (row == null) {

					if (iterationCounter > 0)
						flushBuffers(vsrTable, pairList, sampleDTO.getId(), ops, zygoList, iterationCounter);

					break;
				}

				row[ZYGOSITY_INDEX] = row[ZYGOSITY_INDEX].trim();
				if (row[ZYGOSITY_INDEX].equals("hom"))
					zygoList.add(Zygosity.HOMOZYGOUS);
				else if (row[ZYGOSITY_INDEX].equals("het"))
					zygoList.add(Zygosity.HETEROZYGOUS);
				else if (row[ZYGOSITY_INDEX].equals("hem"))
					zygoList.add(Zygosity.HEMIZYGOUS);
				else if (row[ZYGOSITY_INDEX].equals("het-com"))
					zygoList.add(Zygosity.HET_COM);

				// newVariantMini = new VariantMini();
				newUpdate = new Update();

				long position = 0;
				try {
					position = VariantUtil.getAbsolutePosition(Chromosome.parse(row[CHR_INDEX]),
							Integer.parseInt(row[START_INDEX]));
				} catch (ChromosomeRelativeToAbsolutePositionConversionException e) {
					e.printStackTrace();
					// TODO What to do in this case?
					return;
				}

				// newVariantMini.setPosition(position);
				// miniVariants.add(newVariantMini);

				newUpdate.set(FIELD_NAME_POSITION, position);

				if (!StringUtils.isBlank(row[REF_INDEX])) {
					row[REF_INDEX] = row[REF_INDEX].trim();
					if (!dot.equals(row[REF_INDEX]))
						newUpdate.set(FIELD_NAME_REF, row[REF_INDEX]);
				}

				if (!StringUtils.isBlank(row[ALT_INDEX])) {
					row[ALT_INDEX] = row[ALT_INDEX].trim();
					if (!dot.equals(row[ALT_INDEX]))
						newUpdate.set(FIELD_NAME_ALT, row[ALT_INDEX]);
				}

				if (!StringUtils.isBlank(row[ENS_GENE_GENE_INDEX])) {
					row[ENS_GENE_GENE_INDEX] = row[ENS_GENE_GENE_INDEX].trim();
					if (!dot.equals(row[ENS_GENE_GENE_INDEX])) {
						int firstColonPosition = row[ENS_GENE_GENE_INDEX].indexOf("|");
						if (firstColonPosition != -1)
							newUpdate.set(FIELD_NAME_ENS_GENE, row[ENS_GENE_GENE_INDEX].substring(0, firstColonPosition));
						else
							newUpdate.set(FIELD_NAME_ENS_GENE, row[ENS_GENE_GENE_INDEX]);
					}
				}

				if (!StringUtils.isBlank(row[REF_GENE_INDEX])) {
					row[REF_GENE_INDEX] = row[REF_GENE_INDEX].trim();
					if (!dot.equals(row[REF_GENE_INDEX]))
						newUpdate.set(FIELD_NAME_REF_GENE, row[REF_GENE_INDEX]);
				}

				if (!StringUtils.isBlank(row[REF_GENE_FUNC_INDEX])) {
					row[REF_GENE_FUNC_INDEX] = row[REF_GENE_FUNC_INDEX].trim();
					if (!dot.equals(row[REF_GENE_FUNC_INDEX]))
						newUpdate.set(FIELD_NAME_REF_GENE_FUNC, row[REF_GENE_FUNC_INDEX]);
				}

				if (!StringUtils.isBlank(row[REF_GENE_EXONIC_FUNC])) {
					row[REF_GENE_EXONIC_FUNC] = row[REF_GENE_EXONIC_FUNC].trim();
					if (!dot.equals(row[REF_GENE_EXONIC_FUNC]))
						newUpdate.set(FIELD_NAME_REF_GENE_EXONIC_FUNC, row[REF_GENE_EXONIC_FUNC]);
				}
				if (!StringUtils.isBlank(row[REF_GENE_AA_CHANGE_INDEX])) {
					row[REF_GENE_AA_CHANGE_INDEX] = row[REF_GENE_AA_CHANGE_INDEX].trim();
					if (!dot.equals(row[REF_GENE_AA_CHANGE_INDEX])) {
						String[] splits = row[REF_GENE_AA_CHANGE_INDEX].split(",");
						StringBuilder builder = new StringBuilder();
						for (String sp : splits)
							if (sp.indexOf(":") >= 0)
								builder.append(sp.substring(sp.indexOf(":") + 1) + ",");

						newUpdate.set(FIELD_NAME_REF_GENE_AA_CHANGE, builder.toString());
					}
				}

				if (!StringUtils.isBlank(row[ESP6500SIV2_ALL_INDEX])) {
					row[ESP6500SIV2_ALL_INDEX] = row[ESP6500SIV2_ALL_INDEX].trim();
					if (!StringUtils.isEmpty(row[ESP6500SIV2_ALL_INDEX]) && !dot.equals(row[ESP6500SIV2_ALL_INDEX]))
						newUpdate.set(FIELD_NAME_ESP6500SIV2ALL, row[ESP6500SIV2_ALL_INDEX]);
				}

				if (!StringUtils.isBlank(row[THOUSANDG2015AUG_ALL_INDEX])) {
					row[THOUSANDG2015AUG_ALL_INDEX] = row[THOUSANDG2015AUG_ALL_INDEX].trim();
					if (!dot.equals(row[THOUSANDG2015AUG_ALL_INDEX]))
						newUpdate.set(FIELD_NAME_THOUSANDG2015AUG_ALL, row[THOUSANDG2015AUG_ALL_INDEX]);
				}

				if (!StringUtils.isBlank(row[EXAC_ALL_INDEX])) {
					row[EXAC_ALL_INDEX] = row[EXAC_ALL_INDEX].trim();
					if (!dot.equals(row[EXAC_ALL_INDEX]))
						newUpdate.set(FIELD_NAME_EXAC_ALL, row[EXAC_ALL_INDEX]);
				}

				if (!StringUtils.isBlank(row[CG69_INDEX])) {
					row[CG69_INDEX] = row[CG69_INDEX].trim();
					if (!dot.equals(row[CG69_INDEX]))
						newUpdate.set(FIELD_NAME_CG69, row[CG69_INDEX]);
				}

				if (!StringUtils.isBlank(row[SNP142_INDEX])) {
					row[SNP142_INDEX] = row[SNP142_INDEX].trim();
					if (!dot.equals(row[SNP142_INDEX]))
						newUpdate.set(FIELD_NAME_SNP142, row[SNP142_INDEX]);
				}

				if (!StringUtils.isBlank(row[CLINSIG_INDEX])) {
					row[CLINSIG_INDEX] = row[CLINSIG_INDEX].trim();
					if (!dot.equals(row[CLINSIG_INDEX]))
						newUpdate.set(FIELD_NAME_CLINSIG, row[CLINSIG_INDEX]);
				}

				if (!StringUtils.isBlank(row[CLNDBN_INDEX])) {
					row[CLNDBN_INDEX] = row[CLNDBN_INDEX].trim();
					if (!dot.equals(row[CLNDBN_INDEX]))
						newUpdate.set(FIELD_NAME_CLNDBN, row[CLNDBN_INDEX]);
				}

				if (!StringUtils.isBlank(row[CLNACC_INDEX])) {
					row[CLNACC_INDEX] = row[CLNACC_INDEX].trim();
					if (!dot.equals(row[CLNACC_INDEX]))
						newUpdate.set(FIELD_NAME_CLNACC, row[CLNACC_INDEX]);
				}

				if (!StringUtils.isBlank(row[CLNDSDBID_INDEX])) {
					row[CLNDSDBID_INDEX] = row[CLNDSDBID_INDEX].trim();
					if (!dot.equals(row[CLNDSDBID_INDEX]))
						newUpdate.set(FIELD_NAME_CLNDSDBID, row[CLNDSDBID_INDEX]);
				}

				if (!StringUtils.isBlank(row[SIFT_SCORE_INDEX])) {
					row[SIFT_SCORE_INDEX] = row[SIFT_SCORE_INDEX].trim();
					if (!dot.equals(row[SIFT_SCORE_INDEX]))
						newUpdate.set(FIELD_NAME_SIFT_SCORE, row[SIFT_SCORE_INDEX]);
				}

				if (!StringUtils.isBlank(row[POLYPHEN2_HDIV_SCORE_INDEX])) {
					row[POLYPHEN2_HDIV_SCORE_INDEX] = row[POLYPHEN2_HDIV_SCORE_INDEX].trim();
					if (!dot.equals(row[POLYPHEN2_HDIV_SCORE_INDEX]))
						newUpdate.set(FIELD_NAME_POLYPHEN2_HDIV_SCORE, row[POLYPHEN2_HDIV_SCORE_INDEX]);
				}

				if (!StringUtils.isBlank(row[CADD_RAW_INDEX])) {
					row[CADD_RAW_INDEX] = row[CADD_RAW_INDEX].trim();
					if (!dot.equals(row[CADD_RAW_INDEX]))
						newUpdate.set(FIELD_NAME_CADD_RAW, row[CADD_RAW_INDEX]);
				}

				if (!StringUtils.isBlank(row[CADD_PHRED_INDEX])) {
					row[CADD_PHRED_INDEX] = row[CADD_PHRED_INDEX].trim();
					if (!dot.equals(row[CADD_PHRED_INDEX]))
						newUpdate.set(FIELD_NAME_CADD_PHRED, row[CADD_PHRED_INDEX]);
				}

				newQuery = new Query(
						Criteria.where(FIELD_NAME_POSITION).is(position).and(FIELD_NAME_ALT).is(row[ALT_INDEX]));
				pairList.add(new PositionAltPair(position, row[ALT_INDEX]));

				ops.upsert(newQuery, newUpdate);

				iterationCounter++;

				if (zygoList.size() != iterationCounter)
					System.out.println(iterationCounter);

				if (iterationCounter == IMPORT_SIZE) {
					flushBuffers(vsrTable, pairList, sampleDTO.getId(), ops, zygoList, iterationCounter);
					iterationCounter = 0;
				}
			}
			Sample sampleEntity = sampleRepository.findOne(sampleDTO.getId());
			sampleEntity.setImportingStatus(SampleImportingStatus.FINISHED);
			sampleRepository.save(sampleEntity);
		} catch (IOException exception) {
			Sample sampleEntity = sampleRepository.findOne(sampleDTO.getId());
			sampleEntity.setImportingStatus(SampleImportingStatus.FAILED);
			sampleRepository.save(sampleEntity);

			// TODO async cleanup
		} finally {
			vsrTable.close();
			parser.stopParsing();
		}
	}

	// Execute batch upserts in mongodb, then with the returned objectIds batch
	// insert into HBase and batch insert into Hazelcast
	private void flushBuffers(Table vsrTable, List<PositionAltPair> pairList, Long sampleId, BulkOperations ops,
			List<Zygosity> zygosities, int iterationCounter) throws IOException {

		Map<String, Zygosity> vsrPartialAdditions = new HashMap<>(iterationCounter);
		BulkWriteResult results = ops.execute();
		// results.
		List<BulkWriteUpsert> upserts = results.getUpserts();

		List<Put> hbasePuts = new ArrayList<Put>(iterationCounter);

		if (upserts.size() == iterationCounter) {
			Put newPut = null;
			for (int i = 0; i < iterationCounter; i++) {

				ObjectId objId = (ObjectId) upserts.get(i).getId();

				// For Now We Disable Variant in Memory
				// miniVariantsMap.put(new ObjectId(mini.getId()), miniVariants.get(i));
				newPut = new Put(Bytes.toBytes(objId.toHexString()));
				newPut.addColumn(VariantSampleRelationInDiskRepository.VSR_COLUMN_FAMILY_BYTE_ARRAY,
						Bytes.toBytes(sampleId), Bytes.toBytes(zygosities.get(i).getValue()));
				hbasePuts.add(newPut);
				vsrPartialAdditions.put(sampleId + objId.toHexString(), zygosities.get(i));
			}
		} else {
			Map<Integer, String> idMap = new TreeMap<Integer, String>();
			Criteria crit = new Criteria();
			Criteria[] subCrits = new Criteria[iterationCounter];
			for (int i = 0; i < iterationCounter; i++) {
				subCrits[i] = Criteria.where(FIELD_NAME_POSITION).is(pairList.get(i).position).and(FIELD_NAME_ALT)
						.is(pairList.get(i).alt);
			}
			crit = crit.orOperator(subCrits);

			List<VariantMini> variantMinis = mongoTemplate.find(new Query(crit), VariantMini.class);

			for (VariantMini mini : variantMinis)
				idMap.put(pairList.indexOf(new PositionAltPair(mini.getPosition(), mini.getAlt())), mini.getId());

			Put newPut = null;
			for (Map.Entry<Integer, String> entry : idMap.entrySet()) {
				ObjectId objId = new ObjectId(entry.getValue());

				newPut = new Put(Bytes.toBytes(objId.toHexString()));
				newPut.addColumn(VariantSampleRelationInDiskRepository.VSR_COLUMN_FAMILY_BYTE_ARRAY,
						Bytes.toBytes(sampleId), Bytes.toBytes(zygosities.get(entry.getKey()).getValue()));

				hbasePuts.add(newPut);
				vsrPartialAdditions.put(sampleId + objId.toHexString(), zygosities.get(entry.getKey()));
			}
		}

		vsrTable.put(hbasePuts);

		// For Now We Disable Variant in Memory
		// variantInMemoryRepository.addAll(miniVariantsMap);
		vsrInMemoryRepository.addAll(vsrPartialAdditions);
		pairList.clear();
		zygosities.clear();

	}

}
