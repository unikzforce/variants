package ir.genomics.variants.service.util;

public class ChromosomePosition {
	
	private String chr;
	
	private Integer relativePosition;

	public String getChr() {
		return chr;
	}

	public void setChr(String chr) {
		this.chr = chr;
	}

	public Integer getRelativePosition() {
		return relativePosition;
	}

	public void setRelativePosition(Integer relativePosition) {
		this.relativePosition = relativePosition;
	}

}
