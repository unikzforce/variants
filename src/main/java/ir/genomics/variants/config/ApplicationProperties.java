package ir.genomics.variants.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Variants.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
	
 	private String uploadDirectory;	
	
	public String getUploadDirectory() { 		
		return uploadDirectory; 	
	}

	public void setUploadDirectory(String uploadDirectory) {
		this.uploadDirectory = uploadDirectory;
	}
}
