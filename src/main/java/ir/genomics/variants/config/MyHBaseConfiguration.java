package ir.genomics.variants.config;


import java.io.IOException;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

@Configuration
public class MyHBaseConfiguration {

	@Value("${HBASE_ZOOKEEPER_QUORUM:localhost}")
	private String hbaseZookeeperQuorum;

	@Value("${ZOOKEEPER_PORT:2181}")
	private String zookeeperPort;
	
	private final Logger log = LoggerFactory.getLogger(MyHBaseConfiguration.class);
	
	@Bean
	public org.apache.hadoop.conf.Configuration hbaseConfiguration() {
		org.apache.hadoop.conf.Configuration conf = HBaseConfiguration.create();
		
		System.out.println("hbase zookeeper quorum :" + hbaseZookeeperQuorum);
		conf.set("hbase.zookeeper.quorum", hbaseZookeeperQuorum);
		conf.set("hbase.zookeeper.property.clientPort", zookeeperPort);

//		try {
//			HBaseAdmin.checkHBaseAvailable(conf);
//			log.info("HBase Server is Available");
//		} catch (MasterNotRunningException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ZooKeeperConnectionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ServiceException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return conf;
	}
	
	@Bean
	public Connection hbaseConnection() throws IOException {
		return ConnectionFactory.createConnection(hbaseConfiguration());
	}
	
}
