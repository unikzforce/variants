/**
 * View Models used by Spring MVC REST controllers.
 */
package ir.genomics.variants.web.rest.vm;
