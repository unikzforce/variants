package ir.genomics.variants.web.rest;

import com.codahale.metrics.annotation.Timed;
import ir.genomics.variants.service.SampleService;
import ir.genomics.variants.web.rest.errors.BadRequestAlertException;
import ir.genomics.variants.web.rest.util.HeaderUtil;
import ir.genomics.variants.web.rest.util.PaginationUtil;
import ir.genomics.variants.service.dto.SampleDTO;
import ir.genomics.variants.service.dto.SampleWithVariantZygosityDTO;
import io.github.jhipster.web.util.ResponseUtil;

import org.apache.commons.fileupload.FileUploadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

/**
 * REST controller for managing Sample.
 */
@RestController
@RequestMapping("/api")
public class SampleResource {

	private final Logger log = LoggerFactory.getLogger(SampleResource.class);

	private static final String ENTITY_NAME = "sample";

	private final SampleService sampleService;

	public SampleResource(SampleService sampleService) {
		this.sampleService = sampleService;
	}

	@RequestMapping("/samples/upload-sample")
	public ResponseEntity<SampleDTO> uploadSample(HttpServletRequest request)
			throws URISyntaxException, FileUploadException, IOException {
		SampleDTO result = sampleService.uploadSample(request);
		return ResponseEntity.created(new URI("/samples/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	// /**
	// * POST /samples : Create a new sample.
	// *
	// * @param sampleDTO the sampleDTO to create
	// * @return the ResponseEntity with status 201 (Created) and with body the new
	// sampleDTO, or with status 400 (Bad Request) if the sample has already an ID
	// * @throws URISyntaxException if the Location URI syntax is incorrect
	// */
	// @PostMapping("/samples")
	// @Timed
	// public ResponseEntity<SampleDTO> createSample(@RequestBody SampleDTO
	// sampleDTO) throws URISyntaxException {
	// log.debug("REST request to save Sample : {}", sampleDTO);
	// if (sampleDTO.getId() != null) {
	// throw new BadRequestAlertException("A new sample cannot already have an ID",
	// ENTITY_NAME, "idexists");
	// }
	// SampleDTO result = sampleService.save(sampleDTO);
	// return ResponseEntity.created(new URI("/api/samples/" + result.getId()))
	// .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME,
	// result.getId().toString()))
	// .body(result);
	// }

	/**
	 * PUT /samples : Updates an existing sample.
	 *
	 * @param sampleDTO
	 *            the sampleDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         sampleDTO, or with status 400 (Bad Request) if the sampleDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the sampleDTO
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/samples")
	@Timed
	public ResponseEntity<SampleDTO> updateSample(@RequestBody SampleDTO sampleDTO) throws URISyntaxException {
		log.debug("REST request to update Sample : {}", sampleDTO);
		SampleDTO result = sampleService.save(sampleDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sampleDTO.getId().toString())).body(result);
	}

	/**
	 * GET /samples : get all the samples.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of samples in
	 *         body
	 */
	@GetMapping("/samples")
	@Timed
	public ResponseEntity<List<SampleDTO>> getAllSamples(Pageable pageable) {
		log.debug("REST request to get a page of Samples");
		Page<SampleDTO> page = sampleService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/samples");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/samples/variant-disease-info")
	@Timed
	public ResponseEntity<List<SampleWithVariantZygosityDTO>> getAllEngagingSamples(@RequestParam("variantId") String variantId,
			@RequestParam("diseaseId") Long diseaseId) {
		log.debug("REST request to get all samples engaging variantId and dseaseId");
		List<SampleWithVariantZygosityDTO> dtos = sampleService.findAllEngaging(variantId, diseaseId);
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	/**
	 * GET /samples/:id : get the "id" sample.
	 *
	 * @param id
	 *            the id of the sampleDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the sampleDTO,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/samples/{id}")
	@Timed
	public ResponseEntity<SampleDTO> getSample(@PathVariable Long id) {
		log.debug("REST request to get Sample : {}", id);
		SampleDTO sampleDTO = sampleService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(sampleDTO));
	}

	/**
	 * DELETE /samples/:id : delete the "id" sample.
	 *
	 * @param id
	 *            the id of the sampleDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/samples/{id}")
	@Timed
	public ResponseEntity<Void> deleteSample(@PathVariable Long id) {
		log.debug("REST request to delete Sample : {}", id);
		sampleService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

}
