package ir.genomics.variants.web.rest;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import ir.genomics.variants.repository.exception.CorruptVariantSampleRelationException;
import ir.genomics.variants.service.VariantService;
import ir.genomics.variants.service.dto.VariantDTO;
import ir.genomics.variants.service.dto.VariantSampleDiseasePrevalenceDTO;
import ir.genomics.variants.service.dto.VariantSearchResultDTO;
import ir.genomics.variants.service.impl.exception.InvalidSearchPhraseException;
import ir.genomics.variants.service.util.exception.ChromosomeRelativeToAbsolutePositionConversionException;
import ir.genomics.variants.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Variant.
 */
@RestController
@RequestMapping("/api")
public class VariantResource {
	
    private final Logger log = LoggerFactory.getLogger(VariantResource.class);
    
    private static final String ENTITY_NAME = "variant";
    
    private final VariantService variantService;
    
    public VariantResource(VariantService variantService) {
    	this.variantService = variantService;
    }
    
    @GetMapping("/variants/search")
    @Timed
	public ResponseEntity<List<VariantSearchResultDTO>> searchVariants(@ApiParam Pageable pageable,
			@RequestParam(name = "variant-search-phrase", required = true) String variantSearchPhrase,
			@RequestParam(name = "sample-age-start", required = false) Integer sampleAgeStart,
			@RequestParam(name = "sample-age-end", required = false) Integer sampleAgeEnd,
			@RequestParam(name = "sample-genders[]", required = false) Integer[] sampleGenders,
			@RequestParam(name = "sample-birth-provinces[]", required = false) Integer[] sampleBirthProvinces,
			@RequestParam(name = "sample-disease-ids[]", required = false) Long[] sampleDiseaseIds,
			@RequestParam(name = "sample-race-ids[]", required=false) Long[] sampleRaceIds) throws InvalidSearchPhraseException {
		Page<VariantSearchResultDTO> page = variantService.search(pageable, variantSearchPhrase, sampleAgeStart, sampleAgeEnd, sampleGenders, sampleBirthProvinces, sampleDiseaseIds, sampleRaceIds);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/variants/search");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    
    @GetMapping("/variants/{id}")
    @Timed
    public ResponseEntity<VariantDTO> getVariant(@PathVariable String id) {
    	log.debug("REST request to get Variant : {}", id);
    	VariantDTO variantDTO = variantService.findOne(id);
    	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(variantDTO));
    }
    
    @GetMapping("/variants/{id}/extra")
    @Timed
    public ResponseEntity<VariantSampleDiseasePrevalenceDTO> getVariantExtra(@PathVariable String id) throws CorruptVariantSampleRelationException {
    	log.debug("REST request to get Variant Sample Disease prevalences: {}", id);
    	VariantSampleDiseasePrevalenceDTO result = variantService.findOnePrevalenceDTO(id);
    	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));
    }
}
