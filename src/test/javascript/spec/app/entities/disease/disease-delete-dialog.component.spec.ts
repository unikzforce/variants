/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { VariantsTestModule } from '../../../test.module';
import { DiseaseDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/disease/disease-delete-dialog.component';
import { DiseaseService } from '../../../../../../main/webapp/app/entities/disease/disease.service';

describe('Component Tests', () => {

    describe('Disease Management Delete Component', () => {
        let comp: DiseaseDeleteDialogComponent;
        let fixture: ComponentFixture<DiseaseDeleteDialogComponent>;
        let service: DiseaseService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [VariantsTestModule],
                declarations: [DiseaseDeleteDialogComponent],
                providers: [
                    DiseaseService
                ]
            })
            .overrideTemplate(DiseaseDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DiseaseDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DiseaseService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
