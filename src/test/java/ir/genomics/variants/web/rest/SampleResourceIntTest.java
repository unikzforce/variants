package ir.genomics.variants.web.rest;

import ir.genomics.variants.VariantsApp;
import ir.genomics.variants.service.SampleService;
import ir.genomics.variants.service.dto.SampleDTO;
import ir.genomics.variants.service.mapper.SampleMapper;
import ir.genomics.variants.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ir.genomics.variants.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ir.genomics.variants.domain.Sample;
import ir.genomics.variants.domain.enumeration.Gender;
import ir.genomics.variants.domain.enumeration.Province;
import ir.genomics.variants.domain.enumeration.SampleImportingStatus;
import ir.genomics.variants.repository.sample.SampleRepository;
/**
 * Test class for the SampleResource REST controller.
 *
 * @see SampleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = VariantsApp.class)
public class SampleResourceIntTest {

    private static final String DEFAULT_FILE_ORIGINAL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_ORIGINAL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_HASH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_HASH_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_AGE = 1;
    private static final Integer UPDATED_AGE = 2;

    private static final String DEFAULT_NATIONAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_NATIONAL_CODE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BIRTH_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTH_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DEATH_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DEATH_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final Province DEFAULT_BIRTH_PROVINCE = Province.TEHRAN;
    private static final Province UPDATED_BIRTH_PROVINCE = Province.ALBORZ;

    private static final Province DEFAULT_LIVING_PROVINCE = Province.TEHRAN;
    private static final Province UPDATED_LIVING_PROVINCE = Province.ALBORZ;

    private static final SampleImportingStatus DEFAULT_IMPORTING_STATUS = SampleImportingStatus.NOT_STARTED;
    private static final SampleImportingStatus UPDATED_IMPORTING_STATUS = SampleImportingStatus.NOT_FINISHED;

    @Autowired
    private SampleRepository sampleRepository;

    @Autowired
    private SampleMapper sampleMapper;

    @Autowired
    private SampleService sampleService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSampleMockMvc;

    private Sample sample;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SampleResource sampleResource = new SampleResource(sampleService);
        this.restSampleMockMvc = MockMvcBuilders.standaloneSetup(sampleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sample createEntity(EntityManager em) {
        Sample sample = new Sample()
            .fileOriginalName(DEFAULT_FILE_ORIGINAL_NAME)
            .fileHashName(DEFAULT_FILE_HASH_NAME)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .age(DEFAULT_AGE)
            .nationalCode(DEFAULT_NATIONAL_CODE)
            .birthDate(DEFAULT_BIRTH_DATE)
            .deathDate(DEFAULT_DEATH_DATE)
            .gender(DEFAULT_GENDER)
            .birthProvince(DEFAULT_BIRTH_PROVINCE)
            .livingProvince(DEFAULT_LIVING_PROVINCE)
            .importingStatus(DEFAULT_IMPORTING_STATUS);
        return sample;
    }

    @Before
    public void initTest() {
        sample = createEntity(em);
    }

    @Test
    @Transactional
    public void createSample() throws Exception {
        int databaseSizeBeforeCreate = sampleRepository.findAll().size();

        // Create the Sample
        SampleDTO sampleDTO = sampleMapper.toDto(sample);
        restSampleMockMvc.perform(post("/api/samples")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sampleDTO)))
            .andExpect(status().isCreated());

        // Validate the Sample in the database
        List<Sample> sampleList = sampleRepository.findAll();
        assertThat(sampleList).hasSize(databaseSizeBeforeCreate + 1);
        Sample testSample = sampleList.get(sampleList.size() - 1);
        assertThat(testSample.getFileOriginalName()).isEqualTo(DEFAULT_FILE_ORIGINAL_NAME);
        assertThat(testSample.getFileHashName()).isEqualTo(DEFAULT_FILE_HASH_NAME);
        assertThat(testSample.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testSample.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testSample.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testSample.getNationalCode()).isEqualTo(DEFAULT_NATIONAL_CODE);
        assertThat(testSample.getBirthDate()).isEqualTo(DEFAULT_BIRTH_DATE);
        assertThat(testSample.getDeathDate()).isEqualTo(DEFAULT_DEATH_DATE);
        assertThat(testSample.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testSample.getBirthProvince()).isEqualTo(DEFAULT_BIRTH_PROVINCE);
        assertThat(testSample.getLivingProvince()).isEqualTo(DEFAULT_LIVING_PROVINCE);
        assertThat(testSample.getImportingStatus()).isEqualTo(DEFAULT_IMPORTING_STATUS);
    }

    @Test
    @Transactional
    public void createSampleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sampleRepository.findAll().size();

        // Create the Sample with an existing ID
        sample.setId(1L);
        SampleDTO sampleDTO = sampleMapper.toDto(sample);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSampleMockMvc.perform(post("/api/samples")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sampleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sample in the database
        List<Sample> sampleList = sampleRepository.findAll();
        assertThat(sampleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSamples() throws Exception {
        // Initialize the database
        sampleRepository.saveAndFlush(sample);

        // Get all the sampleList
        restSampleMockMvc.perform(get("/api/samples?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sample.getId().intValue())))
            .andExpect(jsonPath("$.[*].fileOriginalName").value(hasItem(DEFAULT_FILE_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].fileHashName").value(hasItem(DEFAULT_FILE_HASH_NAME.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].nationalCode").value(hasItem(DEFAULT_NATIONAL_CODE.toString())))
            .andExpect(jsonPath("$.[*].birthDate").value(hasItem(DEFAULT_BIRTH_DATE.toString())))
            .andExpect(jsonPath("$.[*].deathDate").value(hasItem(DEFAULT_DEATH_DATE.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].birthProvince").value(hasItem(DEFAULT_BIRTH_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].livingProvince").value(hasItem(DEFAULT_LIVING_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].importingStatus").value(hasItem(DEFAULT_IMPORTING_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getSample() throws Exception {
        // Initialize the database
        sampleRepository.saveAndFlush(sample);

        // Get the sample
        restSampleMockMvc.perform(get("/api/samples/{id}", sample.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sample.getId().intValue()))
            .andExpect(jsonPath("$.fileOriginalName").value(DEFAULT_FILE_ORIGINAL_NAME.toString()))
            .andExpect(jsonPath("$.fileHashName").value(DEFAULT_FILE_HASH_NAME.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.nationalCode").value(DEFAULT_NATIONAL_CODE.toString()))
            .andExpect(jsonPath("$.birthDate").value(DEFAULT_BIRTH_DATE.toString()))
            .andExpect(jsonPath("$.deathDate").value(DEFAULT_DEATH_DATE.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.birthProvince").value(DEFAULT_BIRTH_PROVINCE.toString()))
            .andExpect(jsonPath("$.livingProvince").value(DEFAULT_LIVING_PROVINCE.toString()))
            .andExpect(jsonPath("$.importingStatus").value(DEFAULT_IMPORTING_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSample() throws Exception {
        // Get the sample
        restSampleMockMvc.perform(get("/api/samples/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSample() throws Exception {
        // Initialize the database
        sampleRepository.saveAndFlush(sample);
        int databaseSizeBeforeUpdate = sampleRepository.findAll().size();

        // Update the sample
        Sample updatedSample = sampleRepository.findOne(sample.getId());
        // Disconnect from session so that the updates on updatedSample are not directly saved in db
        em.detach(updatedSample);
        updatedSample
            .fileOriginalName(UPDATED_FILE_ORIGINAL_NAME)
            .fileHashName(UPDATED_FILE_HASH_NAME)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .age(UPDATED_AGE)
            .nationalCode(UPDATED_NATIONAL_CODE)
            .birthDate(UPDATED_BIRTH_DATE)
            .deathDate(UPDATED_DEATH_DATE)
            .gender(UPDATED_GENDER)
            .birthProvince(UPDATED_BIRTH_PROVINCE)
            .livingProvince(UPDATED_LIVING_PROVINCE)
            .importingStatus(UPDATED_IMPORTING_STATUS);
        SampleDTO sampleDTO = sampleMapper.toDto(updatedSample);

        restSampleMockMvc.perform(put("/api/samples")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sampleDTO)))
            .andExpect(status().isOk());

        // Validate the Sample in the database
        List<Sample> sampleList = sampleRepository.findAll();
        assertThat(sampleList).hasSize(databaseSizeBeforeUpdate);
        Sample testSample = sampleList.get(sampleList.size() - 1);
        assertThat(testSample.getFileOriginalName()).isEqualTo(UPDATED_FILE_ORIGINAL_NAME);
        assertThat(testSample.getFileHashName()).isEqualTo(UPDATED_FILE_HASH_NAME);
        assertThat(testSample.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testSample.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testSample.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testSample.getNationalCode()).isEqualTo(UPDATED_NATIONAL_CODE);
        assertThat(testSample.getBirthDate()).isEqualTo(UPDATED_BIRTH_DATE);
        assertThat(testSample.getDeathDate()).isEqualTo(UPDATED_DEATH_DATE);
        assertThat(testSample.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testSample.getBirthProvince()).isEqualTo(UPDATED_BIRTH_PROVINCE);
        assertThat(testSample.getLivingProvince()).isEqualTo(UPDATED_LIVING_PROVINCE);
        assertThat(testSample.getImportingStatus()).isEqualTo(UPDATED_IMPORTING_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingSample() throws Exception {
        int databaseSizeBeforeUpdate = sampleRepository.findAll().size();

        // Create the Sample
        SampleDTO sampleDTO = sampleMapper.toDto(sample);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSampleMockMvc.perform(put("/api/samples")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sampleDTO)))
            .andExpect(status().isCreated());

        // Validate the Sample in the database
        List<Sample> sampleList = sampleRepository.findAll();
        assertThat(sampleList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSample() throws Exception {
        // Initialize the database
        sampleRepository.saveAndFlush(sample);
        int databaseSizeBeforeDelete = sampleRepository.findAll().size();

        // Get the sample
        restSampleMockMvc.perform(delete("/api/samples/{id}", sample.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Sample> sampleList = sampleRepository.findAll();
        assertThat(sampleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sample.class);
        Sample sample1 = new Sample();
        sample1.setId(1L);
        Sample sample2 = new Sample();
        sample2.setId(sample1.getId());
        assertThat(sample1).isEqualTo(sample2);
        sample2.setId(2L);
        assertThat(sample1).isNotEqualTo(sample2);
        sample1.setId(null);
        assertThat(sample1).isNotEqualTo(sample2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SampleDTO.class);
        SampleDTO sampleDTO1 = new SampleDTO();
        sampleDTO1.setId(1L);
        SampleDTO sampleDTO2 = new SampleDTO();
        assertThat(sampleDTO1).isNotEqualTo(sampleDTO2);
        sampleDTO2.setId(sampleDTO1.getId());
        assertThat(sampleDTO1).isEqualTo(sampleDTO2);
        sampleDTO2.setId(2L);
        assertThat(sampleDTO1).isNotEqualTo(sampleDTO2);
        sampleDTO1.setId(null);
        assertThat(sampleDTO1).isNotEqualTo(sampleDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(sampleMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(sampleMapper.fromId(null)).isNull();
    }
}
